# Suivi de stage
Sauvegarde et suivi de ma production durant mon stage de M2R à l'Institut Elie Cartan à Nancy.

Encadrants
- Radu Stoica
- Madalina Deaconu
### Structure du dépôt
- `entretiens/` : compte-rendu d'entretiens de suivi (`.tex` et `.pdf`)
- `code/` : Implémentation de divers algorithmes
- `notes/` : Rédaction d'éléments divers pour référence future

## Bibliographie
cf. `biblio.bib`
