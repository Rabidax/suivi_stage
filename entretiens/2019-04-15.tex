\documentclass[10pt, a4paper]{article}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{mathtools}

\input{../commandes_perso}

\author{Jean-Léon HENRY}
\title{Compte rendu d'entretien de suivi}
\date{15 avril 2019}

\begin{document}
\maketitle
Cet entretien s'est concentré sur une présentation détaillée d'ABC Shadow et ses enjeux. Lors du prochain (24 avril) je présenterais ma compréhension de l'algorithme de Robbins-Monro.
\section{ABC Shadow}
Objectif : simuler la loi a posteriori des paramètres qui dans notre contexte a pour expression
\begin{equation*}
  p(\theta|x) = \frac{\exp(\left<\theta, t(x)\right>)}{c(\theta)}p(\theta).
\end{equation*}
Ici la loi a priori des paramètres est, à défaut d'alternative pertinente, une loi uniforme sur l'espace des paramètres $\Theta$,
supposé compact. Ce qui explique qu'elle n'apparaît pas dans les ratios ci-dessous.
\subsection{Metropolis-Hasting idéal}
En notant $\mathcal{Q}$ la loi instrumentale, $y$ les données et $\alpha$ le taux d'acceptation on a:
\begin{align}
  \alpha(\theta\rightarrow\psi) &\coloneqq \min\left\{1,\ \frac{p(\psi|y)}{p(\theta|y)}\frac{\mathcal{Q}\left(\psi\rightarrow\theta\right)}{\mathcal{Q}\left(\theta\rightarrow\psi\right)}\right\}\\
  &= \min\left\{1,\ \exp\left(\left<\psi-\theta,t(y)\right>\right)\frac{c(\theta)}{c(\psi)}\frac{\mathcal{Q}\left(\psi\rightarrow\theta\right)}{\mathcal{Q}\left(\theta\rightarrow\psi\right)}\right\}\label{accept_full}
\end{align}
Cette dernière ligne montre le caractère idéal, dans cette situation, d'un algorithme de Metropolis-Hasting classique.
En effet, les constantes de normalisation sont inconnues.

Les auteurs de \cite{MollerEtAl2006} ont proposé une solution, l'idée étant d'utiliser une variable auxiliaire permettant de faire disparaître
les constantes de normalisation du taux d'acceptation. La question du choix d'une bonne variable auxiliaire qui garantirait de bonnes propriétés
de convergence et de mélange est compliquée.

\subsection{Description de la méthode}
La méthode proposée dans \cite{StoicaEtAl2017} part de cette idée de variable auxiliaire dans Metropolis-Hasting et essaye de contourner les
problèmes de mélange grâce à une méthode de calcul bayésien approché (ABC en anglais). Décrivons ABC Shadow plus en détail.
\subsubsection{Chaîne idéale}
On se donne $\Delta>0$, ainsi que la variable auxiliaire $x\sim p(\cdot|\theta_0)$, pour $\theta_0\in\Theta$ connu. Ainsi équipé, on choisi alors pour loi instrumentale
\begin{equation*}
  \mathcal{Q}(\theta\rightarrow\psi) = \frac{\exp\left<\psi, t(x)\right>/c(\psi)}{I(\theta, \Delta, x)}\indic_{B(\theta, \Delta/2)}(\psi)
\end{equation*}
où $I(\theta, \Delta, x) = \int_{B(\theta, \Delta/2)}\exp\left<\psi, t(x)\right>/c(\psi)d\psi$. Ce choix assure la convergence de la chaîne vers une distribution stationaire. Même si les intégrales $I(\theta, \Delta, x)$ sont aussi difficiles à calculer que le ratio $\frac{c(\theta)}{c(\psi)}$ de départ, ce qui rend la chaîne "idéale", elle plus facile à approximer. C'est de le rôle de la chaîne shadow.
\subsubsection{Chaîne shadow}
\cite[Theorem 1]{StoicaEtAl2017} est le résultat central justifiant la chaîne shadow. En effet ce théorème nous permet
\begin{itemize}
\item d'utiliser une loi uniforme sur $B(\theta, \Delta/2)$ comme loi instrumentale,
  \item  de calculer un taux d'acceptation approché.
\end{itemize}
En effet, si on utilise (avec les notations de \cite[Theorem 1]{StoicaEtAl2017}) $\frac{f(x|\psi)/c(\psi)\indic_{B(\theta, \Delta/2)}(\psi)}{f(x|\theta)/c(\theta)\indic_{B(\psi, \Delta/2)}(\theta)}$ à la place de $\frac{\mathcal{Q}(\theta\rightarrow\psi)}{\mathcal{Q}(\psi\rightarrow\theta)}$ dans \eqref{accept_full}, on voit que les constantes de normalisation se simplifient. Cette chaîne shadow est donc implémentable, ce que l'on paie par la perte de la convergence vers une distribution stationaire.

\subsection{Questions}
Dans cette section j'essaye de me réapproprier les questions soulevées par M. Stoica à propos de sa méthode et de formuler des remarques personelles.
\subsubsection{Convergence}
La question qu'il convient de se poser immédiatement après avoir mis en place ces deux chaînes est celle de la qualité de l'approximation de l'idéale par la shadow.

\cite[Proposition 1]{StoicaEtAl2017} est un premier pas dans ce sens. En effet, si on se donne $\varepsilon>0$ un contrôle, alors pour tout temps $n$ de nos chaînes, leur noyaux de Markov sont uniformément $\varepsilon$-proches, du moment qu'on fait décroitre $\Delta$ suffisamment vite à mesure que $n$ croît.

Mais comment ce résultat s'applique-t'il à l'algorithme implémentant cette méthode \cite[Algorithm 4]{StoicaEtAl2017} ?
En effet, dans cet algorithme, $\Delta$ est fixé et on itère le processus un nombre fini de fois avant de rafraîchir la variable auxiliaire.
Je le comprends de la manière suivante. On ne peut faire tendre $n\rightarrow\infty$ et $\Delta\rightarrow0$ simultanément,
comme \cite[Proposition 1]{StoicaEtAl2017} pourrait le laisser penser. Cela reviendrait à changer la loi instrumentale durant un algorithme
de Metropolis-Hasting. \cite[Algorithm 4]{StoicaEtAl2017} ne fait tourner l'algorithme qu'un nombre fini d'étapes, par valeurs de $x$,
pour que la borne sur $\Delta$ fournie par \cite[Proposition 1]{StoicaEtAl2017} ne deviennent pas trop petite, et donc qu'on perde le contrôle.

Il y aurait donc un compromis à atteindre entre le paramètre $\Delta$ et le nombre d'étapes $n$ (notations de \cite[Algorithm 4]{StoicaEtAl2017}). Comme on réinitialise la valeur de $x$ après $n$ étapes, si ce dernier est trop grand, la divergence apportée par la chaîne shadow peut l'emporter sur la chaîne idéale. Ce problème m'a semblé clair sur le moment, mais j'ai du mal à le formuler plus précisement.

\subsubsection{Choix de la variable auxiliaire}
\cite{StoicaEtAl2017} choisit la variable auxiliaire selon la même loi paramètrique que les données, en utilisant le paramètre obtenu à l'itération précédente. Rien a priori ne force ce choix. Peut-être est-il possible de le relaxer (en tirant $x$ de manière indépendante de $\theta$ par exemple) ? Ou pas ? C'est une question à étudier plus avant.

\bibliographystyle{plain}
\bibliography{../biblio}
\end{document}
