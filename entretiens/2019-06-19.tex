\documentclass[10pt, a4paper]{article}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{hyperref}
\usepackage{graphicx}

\input{../commandes_perso.tex}

\author{Jean-Léon \bsc{Henry}}
\title{Compte rendu de suivi}
\date{19 juin 2019}

\begin{document}
\maketitle
\section{Récapitulé de la situation}
Dans ce qui suit, les notations non explicites proviennent toutes de \cite[Section 3.2]{StoicaEtAl2017}.

La semaine dernière, j'ai présenté une condition qui permet, à $n$ et $\Delta$ fixés de garder un contrôle constant sur l'écart $\sup_\Theta\left\|P_i^n(\theta,\cdot)-P_s^n(\theta,\cdot)\right\|$ entre les noyaux de transition idéal et shadow.

En effet, \cite[Proof of Proposition 1]{StoicaEtAl2017} montre que pour $n$ et $\Delta$ fixés on a, sous certaines hypothèses sur $x$ qui généré selon la loi $p$ du modèle, que
\begin{equation}
  \label{prop1}
  \sup_{\theta\in\Theta}\left\|P_i^n(\theta,\cdot)-P_s^n(\theta,\cdot)\right\|\leq C_3(x,p,\Theta)n\Delta
\end{equation}
où $C_3(x,p,\Theta)$ est définie par
\begin{equation}
  \label{def_C3}
  C_3(x,p,\Theta) = 3\frac{1}{m(x)}\sup_{\theta\in\Theta}\left\|D_\Theta p(x|\theta)\right\|+2\frac{M(x)}{m(x)^2}\sup_{\theta\in\Theta}\left\|D_\Theta p(x|\theta)\right\|.
\end{equation}
Dans cette expression, on a noté $m(x)$ le minimum de vraisemblance $\inf_{\theta\in\Theta}p(x|\theta)$, $M(x)$ le maximum de vraisemblance et $D_\Theta$ la différentielle par rapport à $\theta$.
Dans le cas des modèles exponentiels, avec une statistique exhaustive $t$, le gradient de $p(x|\cdot)$ a pour expression
\begin{equation}
  \nabla_\Theta p(x|\theta) = p(x|\theta)\left(t(x)-\E_\theta [t(X)]\right).
\end{equation}
Donc \eqref{def_C3} peut se réécrire
\begin{equation}
  C_3(x,p,\Theta) = 3\frac{M(x)}{m(x)}\sup_{\theta\in\Theta}\left\|t(x)-\E_\theta [t(X)]\right\|+2\frac{M(x)^2}{m(x)^2}\sup_{\theta\in\Theta}\left\|t(x)-\E_\theta [t(X)]\right\|.
\end{equation}

Le problème de \eqref{prop1}, c'est que $C_3$ dépend de $x$ et donc quand on réactualise la variable auxiliaire, la majoration évolue. Si on pouvait majorer $C_3$ indépendamment de la variable auxiliaire, le problème serait résolu. Cependant dans le cas de nos modèles, une des composantes de $t(x)$ est le nombre $n(x)$ de points de la configuration $x$, qui peut a priori être aussi grand que désiré. Donc majorer $C_3$ indépendamment de $x$ paraît compliqué.

\section{Contribution de la semaine passée}
L'idée qui a émergé la semaine dernière était de d'essayer de contrôler le comportement de $C_3$ en fonction de la variable auxiliaire $x$ de manière probabiliste.
\subsection{Expérience numérique}
Le fichier \verb|controleABC_expon.py| du dossier \verb|code| dans le dépôt contient un petit script qui explore cette problématique. Le code est Python 3 et contient en préambule des commentaires décrivant les grandes lignes de son fonctionnement.

J'ai pris pour $p(\cdot|\theta)$ la loi exponentielle de paramètre $\theta$. J'ai choisi de manière ad hoc un espace des paramètres compact de la forme $[a,b]$. Dans le code, $a=0.1, b=10$. Cet espace est discrétisé et on choisi un paramètre de manière uniforme pour générer un nombre fixé de valeurs de $x$. Pour finir, les trois grandeurs $M/m$, $\sup_{\theta\in\Theta}\left\|t(x)-\E_\theta [t(X)]\right\|$ et $C_3$ sont tracées en fonction de $x$. Une sortie typique du programme est donnée en figure.

\begin{figure}
  \centering
  \includegraphics[scale=0.5]{19juin_2}
  \caption{Sortie du code}
  
\end{figure}

Pour des raisons de lisibilité, le code cache certains points dus à des valeurs de $x$ trop grandes, mais affiche le nombre de points cachés, d'où le titre du graphique. Ce comportement peut être désactivé en remplaçant l'expression de \verb|plot_too_big| par \verb|False|. Ces valeurs trop grandes de $x$ font diverger $C_3$ car $m(x)$ devient quasi nul.

On voit que les valeurs de $x$ semble globalement s'accumuler près d'un minimum, ce qui est relativement encourageant. On peut espérer majorer $C_3$ en moyenne, par exemple.

\subsection{Théorie}
De ce point de vue, j'ai un peu avancé dans le contrôle de $\sup_{\theta\in\Theta}\left\|t(x)-\E_\theta [t(X)]\right\|$. J'ai cherché, comme l'évoquait M. Stoica des résultats sur la distribution du score $t(x)-\E_\theta [t(X)]$, mais sans grand succès.

Mon idée est la suivante : pour les trois modèles à interactions présentés dans \cite{Muzzolon2017}, la statistique exhaustive a respectivement pour expression (avec les notations du rapport) : 
\begin{itemize}
\item[Poisson:] $t(x)=n(x)$,
\item[Strauss:] $t(x)=(n(x), s_r(x))$,
\item[Interaction par aires:] $t(x)=(n(x), -C_r(x))$.
\end{itemize}
Dans tous ces cas, $t$ est à composantes positives et on peut les majorer en fonction de $n(x)$. Par exemple, pour le modèle d'interaction par aires :
\begin{align}
  &0\leq A_r(x) \leq n(x)\pi r^2.\\
  \intertext{Ce qui implique que}
  &0\leq -C_r(x) \leq n(x).\label{maj_cr}
\end{align}
On en déduit
\begin{align}
  \E\left\|t(x)-\E[t(x)]\right\|^2 &= \Var[n(x)]+\Var[-C_r(x)]\\
   &\leq \Var[n(x)]+\Var[n(x)]+\E[n(x)]^2\label{mini_res}
\end{align}
en utilisant \eqref{maj_cr}. Ce qui est intéressant car en notant $S(x)$ la quantité
\begin{equation}
  \sup_{\theta\in\Theta}\left\|t(x)-\E_\theta [t(X)]\right\|,
\end{equation}
on a que
\begin{align}
  \E S(x) &\leq \E\left[\left\|t(x)-\E t(x)\right\|+\sup_{\theta\in\Theta}\left\|\E t(x)-\E_\theta[t(X)]\right\|\right].
  \intertext{En utilisant l'inégalité de Jensen et \eqref{mini_res}, on peut majorer le premier terme. Le deuxième terme se majore simplement indépendamment de $x$. On a en effet}
  \E S(x)&\leq \sqrt{2\Var[n(x)]+\E[n(x)]^2}+2\sup_{\theta\in\Theta}\left\|\E_\theta[t(X)]\right\|.
\end{align}
Pour le modèle de Strauss, une majoration du type \eqref{mini_res} est aussi disponible. Pour arriver à une majoration en moyenne de $C_3$, il reste cependant le terme $M(x)/m(x)$.
\bibliographystyle{plain}
\bibliography{../biblio}
\end{document}
