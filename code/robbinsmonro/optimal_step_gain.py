#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Illustration de l'optimalité du gain du pas en 1/q sur une loi exponentielle
"""

import numpy as np
import matplotlib.pyplot as plt

# Constantes
rate = 1
quantile = 0.3
theta_true = -np.log(1-quantile)/rate
print(f"Vraie valeur {theta_true}")
q = rate*(1-quantile)
opt_gain = 1/q

# Paramètres
gain = 10
assert gain > 1/(2*q)
print(f"Gain optimal = {opt_gain}")
print(f"Gain = {gain}")
n_iter = 5000

# RM
init = np.random.random()
print(f"Valeur initiale = {init}")
theta_opt = [init]
for i in range(1, n_iter):
    pas = opt_gain/i
    Y = 1 if np.random.exponential(scale=1/rate) <= theta_opt[i-1] else 0
    # Gaffe au signe, ici la fonction croît
    theta_opt.append(theta_opt[i-1] - pas*(Y-quantile))

theta = [init]
for i in range(1, n_iter):
    pas = gain/i
    Y = 1 if np.random.exponential(scale=1/rate) <= theta[i-1] else 0
    # Gaffe au signe, ici la fonction croît
    theta.append(theta[i-1] - pas*(Y-quantile))

# Afficher les résultat
plt.figure(1)
plt.clf()
plt.subplot(211)
plt.plot(theta_opt, label=f"Gain opt = {opt_gain}")
plt.plot([0, n_iter], [theta_true, theta_true], label="Vraie valeur", linestyle="--")
plt.plot([0, n_iter], [init, init], label="Valeur initiale", linestyle="--")
plt.legend()
plt.title(f"quantile {quantile} d'une exponentielle de paramètre {rate}")
plt.subplot(212)
plt.plot(theta, label=f"Gain = {gain}")
plt.plot([0, n_iter], [theta_true, theta_true], label="Vraie valeur", linestyle="--")
plt.plot([0, n_iter], [init, init], label="Valeur initiale", linestyle="--")

plt.legend()
plt.show()

print("DONE".center(50))
