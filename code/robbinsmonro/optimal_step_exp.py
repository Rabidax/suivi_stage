#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Etude de la convergence selon le choix du pas
Illustration sur l'exemple d'une loi exponentielle
Cf. FrayssePhD2013
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats

# Constantes
rate = 10
niveau = 0.2
quantile_true = -np.log(1-niveau)/rate
diff = rate*(1-niveau)
assert diff > 1/2
variance_asymp = (niveau-niveau**2)/(2*diff-1)

# Paramètres
exp_val = [0.51, 0.6, 0.8, 1]
n_iter = 1000
n_samples = 1000
print('\n')
for exp in exp_val:
    samples = np.zeros(n_samples)
    for k in range(n_samples):
        q = np.random.random()
        for i in range(1, n_iter):
            pas = 1.0/(i**exp)
            Y = 1 if np.random.exponential(scale=1/rate) <= q else 0
            # Gaffe au signe, ici la fonction croît
            q = q - pas*(Y-niveau)
        samples[k] = np.sqrt((n_iter-1)**exp)*(q-quantile_true)/np.sqrt(variance_asymp)
    _, fit = stats.probplot(samples)
    slope, inter, rval = fit
    print(f"Exposant : {exp}".ljust(20)+
          "R^2 : {:.5f}".format(rval**2).ljust(15)+
          "Slope : {:.3f}".format(slope).ljust(15)+
          "Inter : {:.3f}".format(inter))
          

print("DONE".center(50))
