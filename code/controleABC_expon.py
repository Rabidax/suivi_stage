#!/usr/bin/python3

"""
Etude du comportement du ratio M/m dans le cas d'une loi exponentielle.
M,m sont respectivement le max et le min de vraisemblance.
La notation C_3 utilisée plus bas vient de l'article sur ABC Shadow.

Le script affiche trois graphes.
- Le comportement de M/m selon x, où x est généré par une loi exponentielle de paramètre choisi de manière uniforme sur l'ensemble des paramètres.
- Le comportement de la quantité \sup_\lambda|t(x)-E_\lambda(t(X))| selon x
- Le comportement de la constante C_3 qui contrôle dans ABC Shadow l'écart entre le noyau idéal et le noyau Shadow

Le script est codé pour Python 3
Bibliothèques Python utilisée + versions :
numpy                        1.16.3             
matplotlib                   3.0.3              
"""

import matplotlib.pyplot as plt
import numpy as np

def density(x, rate):
    return rate*np.exp(-rate*x)

# L'espace des paramètres est choisi compact : \Theta = [rate_inf, rate_sup]
rate_inf = 0.1
rate_sup = 10
print(1/rate_sup, 1/rate_inf)
nb_config = 50  # Nombre d'états à générer pour comparaison

r = np.linspace(rate_inf, rate_sup, num=1000)  # Espace des paramètres

# On génère un certain nombre d'états à comparer
# Numpy paramètrise l'exponentielle par l'inverse de la moyenne, i.e.
# 1/lambda avec les notations classiques
scale = 1/np.random.choice(r)  # paramètre a priori
x_val = np.random.exponential(scale, nb_config)
x_val.sort()


# Min et max de vraisemblance
M = np.array([np.max(density(x, r)) for x in x_val])
m = np.array([np.min(density(x, r)) for x in x_val])

# Sup du gradient de la log-vraisemblance
sup_grad = np.maximum(np.abs(x_val-1/rate_inf), np.abs(x_val-1/rate_sup))


### Affichage ###
plot_too_big = (M/m)[-1] >= (M/m)[0]
if plot_too_big:
    # On a généré des états trops grand, ce qui rend l'affichage peu lisible
    new_ratio = (M/m)[(M/m)<=(M/m)[0]]
    N = len(x_val)-len(new_ratio)  # Nb d'états rejetés des graphiques

# plt.figure(1)
# for x in x_val:
#     plt.plot(r, density(x, r), label=f"{round(x,5)}")
# plt.title(r"$\lambda\mapsto\lambda e^{-\lambda x}$")
# plt.xlabel(r"$\lambda\in$"+f"[{rate_inf},{rate_sup}]")
# plt.legend()

plt.figure(2)
plt.subplot(131)
if not plot_too_big:
    plt.plot(x_val, M/m, label="M/m", marker="*")
else:
    plt.plot(x_val[:-N], new_ratio, label="M/m", marker="*")
plt.title(r"$x\sim$"+f"Exp({round(1/scale, 4)})")
plt.xlabel("x")
plt.axvline(x=scale,color="red", label="moyenne de x")
plt.legend()

plt.subplot(132)
plt.plot(x_val, sup_grad)
plt.xlabel("x")
plt.title(r"$\sup_\lambda|t(x)-E_\lambda(t(X))|$")

plt.subplot(133)
if not plot_too_big:
    plt.plot(x_val, (M/m+(M/m)**2)*sup_grad, "-*")
else:
    plt.plot(x_val[:-N], (3*new_ratio+2*new_ratio**2)*sup_grad[:-N], "-*")
    plt.suptitle(f"{N} états cachés sur {nb_config}")
plt.xlabel("x")
plt.title(r"$C_3$")
plt.axvline(x=scale,color="red", label="moyenne de x")
plt.legend()

plt.show()
