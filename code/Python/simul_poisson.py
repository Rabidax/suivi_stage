#!/usr/bin/env python3

"""
Fournit une fonction pour simuler un ppP dans [0,1]^2 et affiche le
résultat.
Compte les r-voisins et affiche les cercles de rayon r/2
"""

import matplotlib.pyplot as plt
import numpy as np

def poisson(scale):
    """Produit une réalisation d'un ppP homogène dans [0,1]^2"""
    count = np.random.poisson(scale)
    x = np.zeros(count)
    y = np.zeros(count)
    for i in range(count):
        x[i], y[i] = np.random.random(size=2)
    # Déterminer le nb de paires de pts distants d'au plus r
    s = 0
    r = 0.1
    for i in range(count):
        for j in range(i+1, count):
            if (x[i]-x[j])**2+(y[i]-y[j])**2 <= r**2:
                s += 1
    return x, y, s

if __name__ == "__main__":
    data_x, data_y, s = poisson(10)
    plt.figure()
    plt.plot(data_x, data_y, "o")
    # Draw circles
    def circle(z):
        t = np.linspace(0,2*np.pi)
        x = z[0] + 0.05*np.cos(t)
        y = z[1] + 0.05*np.sin(t)
        return x, y
    for center in zip(data_x, data_y):
        x,y = circle(center)
        plt.plot(x,y, 'black')
    plt.title(f'{s} r-voisins')
    plt.show()
