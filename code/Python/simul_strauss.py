#!/usr/bin/env python3

"""
Réalisation et affichage d'un pp de Strauss (interaction par distance)
Une configuration x est un np.array de taille n(x)*2
"""

import matplotlib.pyplot as plt
import numpy as np

def proposal(state):
    """Simulation de nouvelles configurations"""
    return np.vstack((state, np.random.random(size=2)))

def kernel(old, new):
    """Noyau associé à proposal"""
    len_old, len_new = old.shape[0], new.shape[0]
    if len_old < len_new:
        return 1.0
    return 1/(len_old+1)

def h(conf, beta=1.0, gamma=0.5):
    """Densité à simuler, non normalisée"""
    r = 1e-1
    card = conf.shape[0]
    # Déterminer s_r(x) : nbs de paires de pts à distance <= r
    voisins = 0
    x, y = conf[:, 0], conf[:, 1]
    for i in range(card):
        for j in range(i+1, card):
            if (x[i]-x[j])**2+(y[i]-y[j])**2 <= r**2:
                voisins += 1
    return beta**card*gamma**voisins

def metropolis(burnin, num_iter):
    assert burnin < num_iter
    """Return num_iter-burnin states provided by the MH algo"""
    states = []
    current = np.random.random(size=(1, 2))
    for _ in range(num_iter):
        new = proposal(current)
        accept = min([1, h(new)/h(current)*kernel(current, new)/kernel(new, current)])
        if np.random.rand() <= accept:
            current = new
        states.append(current)
    return states[burnin:]


if __name__ == "__main__":
    DATA = metropolis(int(input("Burn in?")), int(input("Nb iter?")))
    # Affichage
    plt.figure()
    plt.plot(DATA[-1][:, 0], DATA[-1][:, 1], '+')
    plt.show()
