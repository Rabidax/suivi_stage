#!/usr/bin/env python3

"""
Implemente l'algorithme de Metropolis-Hasting en 1D
"""

import matplotlib.pyplot as plt
import numpy as np


def proposal(x):
    """Proposer un nouvel état"""
    return np.random.normal(loc=x)

def kernel(x, y):
    """Noyau associé avec proposal"""
    sigma = 1.0
    return np.exp(-0.5*(y-x)**2/sigma**2)/np.sqrt(2*np.pi*sigma**2)

def h(x):
    """Densité à simuler"""
    return 0.5 if np.abs(x) < 1 else 1e-12

def metropolis(burnin, num_iter):
    """Retourne num_iter-burnin états"""
    states = np.zeros(num_iter)
    current = 1.0
    for i in range(num_iter):
        new = proposal(current)
        accept = min([1, h(new)/h(current)*kernel(current, new)/kernel(new, current)])
        if np.random.rand() <= accept:
            current = new
        states[i] = current
    return states[burnin:]


if __name__ == "__main__":
    configuration = metropolis(int(input("Burn in?")), int(input("Nb iteration?")))
    plt.figure()
    plt.hist(configuration,range=(-1,1))
    plt.show()
