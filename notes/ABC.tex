Après le détour théorique du chapitre précédent, nous revenons à notre problème. Ce chapitre présente la méthode d'inférence nommée "ABC Shadow" dont le but est d'échantilloner une "loi des paramètres sachant les données", ce qui justifie l'outillage du chapitre précédent. La référence principale pour ce chapitre est l'article de M. Stoica et ses co-auteurs \cite{StoicaEtAl2017}.
\section{Contexte bayésien}
Soit $y$ des données observées, $\Theta$ l'espace des paramètres du modèle considéré et $p(\cdot|\theta)$ une densité de probabilité du modèle générée par $\theta\in\Theta$. On note
\begin{itemize}
  \item vraisemblance la quantité $p(y|\theta)$,
  \item loi a priori des paramètres la densité $p(\theta)$,
    \item loi a posteriori (ou postérieure) des paramètres $p(\theta|y)$.
\end{itemize}
Ces quantités sont reliées selon
\begin{equation}
  p(\theta|y)\propto p(y|\theta)p(\theta).
\end{equation}
Choisir un modèle c'est se donner $p(\cdot|\theta)$. Estimer le paramètre $\theta$ c'est échantilloner la loi a posteriori sachant les données.
\section{Présentation de la méthode}
Dans la suite on fait l'hypothèse importante de compacité de l'espace des paramètres, noté $\Theta$. On choisit une loi uniforme sur $\Theta$ comme loi a priori.

Comme vu en \ref{algo_mh_def}, l'échantillonage par un algorithme de Metropolis-Hastings de la postérieure nécessite, si on ne prend pas de précautions particulières, de calculer le ratio
\begin{equation}
  \frac{p(\theta|y)}{p(\psi|y)},\ (\theta,\psi)\in\Theta^2.
\end{equation}
Dans le cas des processus ponctuels ceci est souvent impossible car on ne connaît pas la constante de normalisation $c$ du modèle (cf. la remarque en fin de section \ref{section_poisson}), et donc pas non plus le ratio $c(\psi)/c(\theta)$.

La méthode ABC Shadow \cite{StoicaEtAl2017} répond à ce problème grâce à deux chaînes de Markov :
\begin{enumerate}
\item une chaîne dite "idéale" qui a de bonnes propriétés (notamment de convergence) mais qui n'est pas implémentable directement,
  \item une chaîne qui approche la première et qu'on peut implémenter : on la nomme chaîne "shadow".
\end{enumerate}
%% On pourrait théoriquement se passer de la chaîne shadow, mais le méchanisme proposé est trop général et ne garantit en rien de bonnes propriétés de mélange.
\subsection{Chaîne idéale}Sa particularité est d'utiliser une variable auxiliaire. C'est-à-dire que selon un méchanisme à définir, on choisit une valeur $x$ issue du modèle, un paramètre $\Delta>0$ et on choisit pour l'algorithme \ref{algo_mh_def}
\begin{itemize}
\item la densité
\begin{equation}
  q(\theta,\psi)=q_\Delta(\theta,\psi|x)=\frac{p(x|\psi)\indic_{B(\theta,\Delta/2)}(\psi)}{I(\theta,\Delta,x)}\label{instru_ideal}
\end{equation}
où $I(\theta,\Delta,x)$ est une constante de normalisation, comme loi instrumentale,
\item $\pi=p(\cdot|y)$.
\end{itemize}
Ce choix déplace le problème en changeant les constantes de normalisations à évaluer : la constante de normalisation $c$ n'apparaît plus dans \eqref{ratio_mh}
\subsubsection{Ergodicité de la chaîne idéale}
La chaîne idéale d'ABC Shadow est uniformément ergodique pour tout couple $\Delta>0$, sous certaines hypothèses sur $x$. En effet :
\begin{itemize}
\item Sous les hypothèses du théorème 1 de \cite{StoicaEtAl2017}, la densité $p(x|\theta)$ est continue par rapport à $\theta$ et donc la postérieure $p(\cdot|x)$ l'est aussi sur l'espace $\Theta$ des paramètres, supposé compact. Elle est aussi supposée strictement positive, donc la postérieure vérifie les conditions du lemme \ref{ergod_ideal}.
\item La loi instrumentale \eqref{instru_ideal} vérifie aussi les conditions du lemme \ref{ergod_ideal}. $\delta = \Delta/2$ convient et $\varepsilon$ est donné par la continuité de $p(x|\cdot)$. Plus précisément, \[\frac{\inf_\Theta p(x|\theta)}{\sup_\Theta p(x|\theta)}\left|B(0,\Delta/2)\right|\] convient.
\item On a donc l'irréductibilité (donc la récurrence de Harris par le lemme \ref{harris_mh}, ainsi que la positivité) et l'apériodicité grâce au lemme \ref{ergod_ideal}. Comme de plus, l'espace des paramètres est compact, celui-ci est petit et la chaîne est uniformément ergodique. 
\end{itemize}
\subsection{Chaîne shadow}
\label{shadow_chain}
Comme ci-dessus on pose $x$ une réalisation de la loi $p(\cdot|\psi)$ pour $\psi\in\Theta$ à choisir. La chaîne shadow est constitutée par les sorties de l'algorithme \ref{algo_mh_def} où on aurait choisi
\begin{itemize}
\item $\pi=p(\cdot|y)$
\item $q(\theta,\cdot)=\frac{\indic_{B(\theta,\Delta/2)}}{|B(\theta,\Delta/2)|}$
  \item $\alpha(\theta,\psi)=1\wedge\frac{\pi(\psi)}{\pi(\theta)}\frac{p(x|\theta)\indic_{B(\psi,\Delta/2)}(\theta)}{p(x|\psi)\indic_{B(\theta,\Delta/2)}(\psi)}$.
\end{itemize}

On s'écarte donc un petit peu d'un algorithme de Metropolis-Hastings classique (comme décrit plus haut).
\subsection{Algorithme ABC Shadow}
On se donne $n$ entier et $\Delta>0$, ainsi qu'un état initial $\theta_0$. 
\begin{enumerate}
\item Générer $x$ selon $p(\cdot|\theta_0)$
\item Faire $n$ itérations de la chaîne shadow (c.f. \ref{shadow_chain})
\item Retourner la $n$-ème itération notée $\theta_n$
  \item Si besoin d'un autre échantillon, retourner en 1. avec $\theta_0=\theta_n$
\end{enumerate}
Ainsi, on a choisi une manière de générer la variable auxiliaire $x$.
\section{Résultats théoriques}
Nous avons mentionné plus haut que la chaîne shadow approche la chaîne idéale. Nous allons justifier cette affirmation puis, dans le prochain chapitre, essayer de voir ce qu'il en est en terme de distribution asymptotique. Autrement dit, a-t-on un résultat de convergence en loi pour la méthode ABC Shadow ?

%% \subsection{Résultat de l'article original}
Du \cite{StoicaEtAl2017}[Theorem 1] qu'on pourra consulter dans l'article, on tire une conséquence importante :
\begin{prop}
  Soit $(\Delta, x)$ un couple paramètre-variable auxiliaire donnant lieu à une chaîne idéale et une chaîne shadow. On notera $P_i$ et $P_s$ les noyaux respectifs de ces chaînes. Alors il existe une constante $C_3>0$ dépendant de $x$, $p$ et $\Theta$ telle que
  \begin{equation}
    \forall n\in\N, \sup_{\theta\in\Theta}\sup_{A\in\mathcal{B}}|P_i^n(\theta,A)-P_s^n(\theta,A)|<C_3n\Delta.
  \end{equation}
\end{prop}
Avec l'hypothèse supplémentaire mais réaliste de différentiabilité de $p(x|\cdot)$, on peut donner une expression de $C_3$.
\section{Illustration numérique}
Pour illustrer l'algorithme, on va étudier son comportement sur une gaussienne. Et plus précisément la dépendance de ses performances au paramètre $\Delta$.

On génère $10^3$ points d'une gaussienne $\mathcal{N}(2,3^2)$, ce sont nos observations. On prend pour condition initiale $\theta_0=(2,9)$, i.e. les vraies valeurs des paramètres. Le paramètre $n$ est fixé à 500. L'espace des paramètres est le compact $[-100;100]\times[0;200]$. On demande $26\cdot 10^3$ échantillons à l'algorithme et on en garde 1 parmi 25, en abandonnant les 1000 premiers. On répète trois fois l'expérience avec différentes valeurs de $\Delta$.

Le tableau \ref{summary_moy} donne quelques statistiques de la première composante des échantillons obtenus. On remarque que la moyenne des échantillons est assez proche de la valeur du maximum de vraisemblance (i.e. la moyenne empirique) des observations qui vaut 1.765453. L'échantillon semble meilleur pour une valeur de $\Delta$ intermédiaire : ni trop grande, ni trop petite. De manière plus globale, plus $\Delta$ est petit, meilleure est l'approximation, dans le sens où la distribution empirique est plus ressérée autour du maximum de vraisemblance.
\begin{table}
  %% \centering
    \caption{Statistiques des échantillons de la moyenne\label{summary_moy}}
  \begin{tabular}{c|c|c|c|c|c|c|}
  $\Delta$ & Min. & 1er quartile & Médiane & Moyenne &3e quartile & Max.\\
  \hline  
  (0.1,0.1) &-1.0983  &0.2193  &1.9604  &1.7705  &3.3022  &4.5734\\
  \hline
  (0.01,0.05) &1.442   &1.696   &1.763   &1.763   &1.831   &2.091\\
  \hline
  (0.001,0.005) &1.598   &1.766   &1.823   &1.821   &1.879   &2.013\\
  %% \hline
  %% Chaîne idéale &1.474   &1.698   &1.759   &1.762   &1.829   &2.057
  
\end{tabular}
\end{table}

En ce qui concerne la variance, le tableau \ref{summary_var} donne les résultats. Le maximum de vraisemblance pour la variance est 9.038038. Les mêmes remarques que pour la moyenne s'appliquent ici. La nécessité d'une valeur intermédiaire de $\Delta$ s'explique sans doute par un compromis entre bonne approximation et mélange correct.
\begin{table}
  %% \centering
  \caption{Statistiques des échantillons de la variance\label{summary_var}}
  \begin{tabular}{c|c|c|c|c|c|c|}
  $\Delta$ & Min. & 1er quartile & Médiane & Moyenne &3e quartile & Max.\\
  \hline  
  (0.1,0.1) &4.917   &6.570   &7.034   &7.051   &7.529   &9.407\\
  \hline
  (0.01,0.05) &7.921   &8.771   &9.034   &9.071   &9.338  &10.616 \\
  \hline
  (0.001,0.005) &7.870   &8.819   &9.134   &9.100   &9.387  &10.222 \\
  
\end{tabular}
\end{table}

Une autre manière de le comprendre est de regarder la trajectoire dans l'espace des paramètres. Les figures \ref{delta_small} et \ref{delta_big} représentent cette trajectoire pour les deux valeurs extrêmes de $\Delta$ que nous avons considéré. On voit que pour un $\Delta$ trop petit, on explore mal l'espace des paramètres. La figure \ref{delta_mid} représente le cas intermédiaire. On remarque que dans ce cas, les échantillons sont plus centrés autour des vraies valeurs, notamment par rapport au cas où $\Delta$ est trop grand.

\begin{figure}
\centering
\includegraphics[scale=0.5]{param_path_small}
\caption{Trajectoire de l'algorithme pour $\Delta$ faible}
\label{delta_small}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.5]{param_path_mid}
\caption{Trajectoire de l'algorithme pour $\Delta$ intermédiaire}
\label{delta_mid}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.5]{param_path_big}
\caption{Trajectoire de l'algorithme pour $\Delta$ grand}
\label{delta_big}
\end{figure}


