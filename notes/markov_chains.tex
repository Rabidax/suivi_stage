Le chapitre précédent présente une méthode d'inférence se basant sur la vraisemblance. Dans les chapitres qui suivent nous allons nous intéresser à des méthodes bayésiennes, qui nécessitent de pouvoir simuler des lois compliquées. L'algorithme de Metropolis-Hastings est la pierre d'angle de ces méthodes, car il permet d'échantilloner une grande variété de lois assez facilement. Ce chapitre commence par introduire les outils nécessaires à l'utilisation de chaînes de Markov à valeurs dans des espaces généraux, et les applique ensuite à la justification des résultats de l'algorithme de Metropolis-Hastings.
\section{Chaînes de Markov}
\label{gen_mc}
Cette partie est inspirée de \cite{meyn2009markov}. Nous ne voulons bien évidemment pas présenter l'ensemble de la théorie ici, mais plutôt offrir un cheminement logique complet jusqu'à la justification de l'algorithme de Metropolis-Hastings.
\subsection{Définitions}
Soit $(X,\mathcal{X})$ un espace mesurable où $\mathcal{X}$ est dénombrablement engendrée. Typiquement, un sous-ensemble de $\R^d$ muni de la mesure de Lebesgue.
%% noyau de transition
\begin{defn}
  Un \emph{noyau de transition} est une application $P:X\times\mathcal{X}\rightarrow [0,1]$ vérifiant
  \begin{itemize}
  \item $\forall A\in\mathcal{X},\ P(\cdot,A)$ est mesurable
  \item $\forall x\in X,\ P(x,\cdot)$ est une mesure de probabilité sur $(X,\mathcal{X})$.
  \end{itemize}
\end{defn}
Comme mesure, $P$ agit sur une fonction mesurable $f$ selon
\begin{equation*}
  Pf(x) = \int_X f(y)P(x,dy).
\end{equation*}
Une mesure $\nu$ agit sur $P$ en tant que fonction mesurable selon
\begin{equation*}
  \nu P(A) = \int_X P(y,A)d\nu(y).
\end{equation*}
\begin{defn}
  On définit par récurrence le noyau à plusieurs pas $P^n$ par
  \begin{align}
    P^0(x,A) &= \delta_x(A)\\
    P^n(x,A) &= \int_XP^{n-1}(y,A)P(x,dy),\ n\geq 1.
  \end{align}
\end{defn}
\begin{defn}
  Si on se donne une distribution initiale $\mu$ et un noyau de transition $P$ sur $X$, on définit un processus stochastique sur un espace de probabilité $(X^\infty, \mathcal{F}, \P_\mu)$ appelé \emph{chaîne de Markov}.
\end{defn}
Pour $x\in X$, $\P_x$ note le cas $\mu=\delta_x$. $X$ pourra être appelé espace des états, puisque les variables aléatoires sont à valeurs dans celui-ci.
\begin{defn}
  Une mesure $\sigma$-finie $\pi$ est dite \emph{invariante} (pour $P$) si $\pi P=\pi$.
\end{defn}
Autrement dit, si une chaîne $(X_n)$ a une distribution invariante $\pi$, et que pour un $n_0$, $X_{n_0}$ a pour loi $\pi$, alors pour tout $j\geq 0$, $X_{n_0+j}$ a aussi pour loi $\pi$. Rien a priori ne guarantit l'unicité d'une telle mesure quand elle existe.\\
Dans le cas où l'espace des états est discret, on peut facilement créer des exemples : le noyau
\begin{equation}
  \label{mc_noyau_ex}
  \begin{pmatrix}
    0.5&0.5&0\\
    0.5&0.5&0\\
    0&0&1
  \end{pmatrix}
\end{equation}
admet $(1/3,1/3,1/3)$ comme mesure invariante. Pour sortir du cas discret, une marche aléatoire continue sur $\R$ admet la mesure de Lebesgue comme mesure invariante : se référer à \cite{meyn2009markov}[Section 10.5.1] pour les détails.
\begin{defn}
  Une chaîne de Markov $(X_n)$ est dite \emph{$\phi$-irréductible} s'il existe une mesure $\phi$ telle que
  \begin{equation}
    \forall A\in \mathcal{X},\ \phi(A) >0\Rightarrow \forall x\in X,\ \P_x(\tau_A<\infty)>0.
  \end{equation}
  où $\tau_A$ est le temps de (premier) retour dans $A$. Autrement dit on retourne avec probabilité positive dans tout ensemble assez gros.
\end{defn}
Cette notion adapte au cas où $X$ n'est pas discret la notion classique d'irréductibilité. L'idée est toujours de s'assurer que tous les états "communiquent" entre eux, car dans le cas contraire on peut voir émerger plusieurs mesures invariantes distinctes. Le noyau présenté en \eqref{mc_noyau_ex} est un exemple de ces comportements qu'on veut éviter : certains états ne communiquent pas entre eux et on trouve $(1/2,1/2,0)$ comme autre mesure invariante possible 

Après s'être assuré que la chaîne peut visiter tout l'espace, on veut éliminer les comportements instables. D'où la notion suivante.
\begin{defn}
  Une chaîne $\phi$-irréductible est dite \emph{de Harris} si
  \begin{equation}
    \forall A\in \mathcal{X},\ \phi(A) >0\Rightarrow \forall x\in X,\ \P_x(\eta_A=\infty)=1,
  \end{equation}
  où $\eta_A$ est le temps $\sum_{n\geq 1}\indic_{X_n\in A}$ d'occupation de $A$.
\end{defn}
La récurrence de Harris étend la notion classique de récurrence d'une chaîne, habituellement formulée par la finitude en moyenne du temps d'occupation.
\begin{defn}
  Une chaîne $\phi$-irréductible pour laquelle une mesure de probabilité est invariante est dite \emph{positive}.
\end{defn}
\begin{thm}
  Si la chaîne est de Harris et positive, elle admet une unique mesure invariante, qui est une mesure de probabilité.
\end{thm}

Toutes ces notions permettent de se poser sans ambiguité la question de la convergence de la chaîne vers une distribution asymptotique.
\subsection{Résultats asymptotiques}
\begin{thm}
  Si une chaîne est de Harris, positive et apériodique alors en notant $\pi$ sa probabilité invariante et $\mu$ une distribution initiale de la chaîne on a :
  \begin{equation}
    \left\|\mu P^n-\pi\right\|\xrightarrow[n\rightarrow\infty]{}0,
  \end{equation}
  où $\left\|\cdot\right\|$  désigne ici la distance de variation totale.
\end{thm}
 On rappelle la définition de la distance de variation totale entre deux mesures $\mu,\nu$ : $\left\|\mu-\nu\right\|=2\sup_{A\in\mathcal{X}}\left|\mu(A)-\nu(A)\right|$. Le choix $\mu = \delta_x$ donne la convergence depuis toute condition initiale, et on parle d'ergodicité de la chaîne. Si la convergence ci-dessus est vraie indépendamment de la condition initiale, i.e. $\sup_x\left\|P^n(x,\cdot)-\pi\right\|\xrightarrow[n\rightarrow\infty]{}0$, l'ergodicité est dite uniforme.
Et dans ce cas
\begin{thm}
  Supposons que la chaîne $X_n$ de noyau $P$ est uniformément ergodique. Alors il existe deux réels $0<\rho<1$ et $M>0$ tels que 
  \begin{equation}
    \forall x\in X,\ \left\|P^n(x,\cdot)-\pi\right\|\leq M\rho^n.
  \end{equation}
\end{thm}
Donnons un critère d'ergodicité uniforme sous certaines hypothèses : 
\begin{defn}
  Un ensemble $C\in \mathcal{X}  $ est dit \emph{petit} s'il existe $n\in\N^*$ et une mesure $\nu$ non triviale sur $\mathcal{X}$ tels que
  \begin{equation}
    \forall A\in\mathcal{X}, x\in C,\ P^n(x,A)\geq \nu(A).
  \end{equation}
\end{defn}
Attention nous appelons "ensemble petit" ce que \cite{meyn2009markov} appelle "small set", et non ce que les auteurs appellent "petite set".
\begin{thm}
  Une chaîne $\phi$-irréductible et apériodique est uniformément ergodique si l'espace $X$ est petit.
\end{thm}
\section{Metropolis-Hastings}

Soit $\pi$ une densité sur l'espace $X$ (on notera de la même manière la mesure de probabilité associée), et un noyau à densité défini par $Q(x,A)=q(x,y)dy$.
\subsection{Algorithme}
\label{algo_mh_def}
On définit une chaîne de Markov (appelée MHMC pour Metropolis-Hastings Markov Chain) à valeurs dans $X$ par l'algorithme suivant :
\begin{enumerate}
\item Au temps $n$, la chaîne est dans l'état $x_n$. Proposer $y$ un nouvel état selon la loi $q(x_n,\cdot)$.
\item Définir le nouvel état de la chaîne selon
  \begin{equation}
    x_{n+1} = 
    \begin{cases}
      y, & \text{avec probabilité }\alpha(x_n,y)\\
      x_n, & \text{avec probabilité }1-\alpha(x_n,y)
    \end{cases}
  \end{equation}
  où
  \begin{equation}
    \label{ratio_mh}
    \alpha(x,y) = 
    \begin{cases}
      \min\left\{1,\frac{\pi(y)}{\pi(x)}\frac{q(y,x)}{q(x,y)}      \right\},& \pi(x)q(x,y)>0\\
      1,&\ \pi(x)q(x,y)=0.
    \end{cases}
  \end{equation}
\end{enumerate}
\subsection{Propriétés et convergence}
Les lemmes non démontrés ici le sont dans \cite{robert2004monte}.

On note
\begin{equation}
  p(x,y) = 
  \begin{cases}
    q(x,y)\alpha(x,y),& x\neq y\\
    0,& x=y.
  \end{cases}
\end{equation}
Le noyau a pour expression
\begin{equation}
  \label{noyau_mh}
  P(x,A) = \int_Ap(x,y)dy+\delta_x(A)\left(1-\int_Xp(x,y)dy\right).
\end{equation}
On veut montrer l'ergodicité de la chaîne. Il nous faut donc montrer que notre chaîne est
\begin{itemize}
\item de Harris
\item positive
\item et apériodique.
\end{itemize}
\paragraph{Existence d'une mesure invariante}
Ce résultat découle du fait que si le noyau de transition $P$ et une probabilité de densité $\pi$ vérifient
\begin{equation}
  \forall x,y,\ P(x,dy)\pi(dx)=P(y,dx)\pi(dy)\label{balance_eq}
\end{equation}
ce qui veut dire que pour deux mesurables $A$ et $B$, $\pi(\indic_BP(\cdot,A)) = \pi(\indic_AP(\cdot,B))$. On dit dans ce cas que la chaîne est réversible. Et alors $\pi$ est invariante pour $P$ :
\begin{align}
  \pi P(A) &= \int_X P(x,A)d\pi(x)\\
  &= \int_X\int_AP(x,dy)\pi(dx)\\
  &= \int_X\int_AP(y,dx)\pi(dy)\\
  &= \int_A\pi(dy)\int_XP(y,dx)\\
  &= \int_A\pi(dy)=\pi(A).
\end{align}
\begin{lemme}
  Le noyau \eqref{noyau_mh} vérifie \eqref{balance_eq}.
\end{lemme}
\paragraph{Récurrence de Harris}
On a le lemme difficile suivant qui est une conséquence de \cite[Theorem 17.1.5]{meyn2009markov}
\begin{lemme}
  \label{harris_mh}
  Si la chaîne de Markov associée à \eqref{noyau_mh} est $\pi$-irréductible, elle est de Harris.
\end{lemme}

Quant à l'apériodicité et la $\pi$-irréductibilité, des conditions suffisantes sur $q$ permettent de conclure à l'ergodicité de la chaîne dans des cas assez généraux. Par exemple :
\begin{lemme}[\cite{RobertsTweedie1996}]
  \label{ergod_ideal}
  Si sur tous les compacts $K$ de son support $\pi$ est bornée et $\inf_{x\in K}\pi(x)>0$  et si
  \begin{equation}
    \exists \epsilon, \delta>0,\forall x\in X,\  |x-y|\leq\delta\Rightarrow q(y|x)\geq\epsilon
  \end{equation}
  alors la MHMC associée à q et $\pi$ est $\pi$-irréductible et apériodique. De plus, tout compact non-vide est "petit" dans le sens précisé ci-dessus.
\end{lemme}

Dans le cas où l'espace $X$ est compact, ceci nous donne donc l'ergodicité uniforme.
