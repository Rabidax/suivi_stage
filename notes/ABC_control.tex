\documentclass[10pt, a4paper]{article}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{hyperref}
\usepackage{graphicx}
%% \usepackage[left=2cm,right=2cm]{geometry}
\usepackage{array}
\usepackage[shortlabels]{enumitem}

\input{../commandes_perso}

\title{Contrôle ABC}
\author{Jean-Léon \bsc{Henry}}

\begin{document}
\maketitle
\tableofcontents
\section{Introduction}
Notre objectif est d'obtenir un résultat de convergence en loi pour l'algorithme ABC Shadow. Pour le dire autrement, on veut montrer qu'asymptotiquement, on échantillone bien la loi a posteriori des paramètres. Notre approche est la suivante :
\begin{enumerate}
\item Montrer que l'écart (en variation totale) entre la chaîne idéale et la chaîne shadow est bien contrôlé en fonction du pas, et ce peu importe la valeur de la variable auxiliaire
  \item Sous une condition liant le pas $\Delta$ et la taille $n$ des blocs d'itérations, utiliser le résultat précédent et l'ergodicité de la chaîne idéale pour montrer la convergence en loi
\end{enumerate}
\section{Contrôle en moyenne de l'écart des noyaux}

\cite[Proof of Proposition 1]{StoicaEtAl2017} montre qu'on a, pour $n$ et $\Delta$ fixés et sous certaines hypothèses sur $x$ qui est généré selon la loi $p$ du modèle
\begin{equation}
  \label{prop1}
  \sup_{\theta\in\Theta}\left\|P_i^n(\theta,\cdot)-P_s^n(\theta,\cdot)\right\|\leq C_3(x,p,\Theta)n\Delta
\end{equation}
où $C_3(x,p,\Theta)$ est définie par
\begin{equation}
  \label{def_C3}
  C_3(x,p,\Theta) = \frac{1}{m(x)}\left(3+2\frac{M(x)}{m(x)}\right)\sup_{\theta\in\Theta}\left\|\nabla_\theta p(x|\theta)\right\|.
\end{equation}
Dans cette expression, on a noté $m(x)$ le minimum de vraisemblance $\inf_{\theta\in\Theta}p(x|\theta)$, $M(x)$ le maximum de vraisemblance et $\nabla_\theta$ le gradient par rapport à $\theta$.
Dans le cas des modèles exponentiels, avec une statistique exhaustive $t$, le gradient de $p(x|\cdot)$ a pour expression
\begin{equation}
  \nabla_\Theta p(x|\theta) = p(x|\theta)\left(t(x)-\E_\theta [t(X)]\right).
\end{equation}
Donc \eqref{def_C3} peut se réécrire
\begin{equation}
  C_3(x,p,\Theta) = \frac{M(x)}{m(x)}\left(3+2\frac{M(x)}{m(x)}\right)\sup_{\theta\in\Theta}\left\|t(x)-\E_\theta [t(X)]\right\|.
\end{equation}

Le problème de \eqref{prop1}, c'est que $C_3$ dépend de $x$ et donc quand on réactualise la variable auxiliaire, la majoration évolue. On voudrait majorer $C_3$ indépendamment de la variable auxiliaire. On va séparer la majoration (faite en moyenne) en deux termes : $\sup_{\theta\in\Theta}\left\|t(x)-\E_\theta [t(X)]\right\|$ et $M(x)/m(x)$.
\subsection{Hypothèses sur le modèle}
On note $f(x|\theta)$ la densité non normalisée.
\begin{enumerate}[(H1)]
\item $\forall \theta,\ \exists M_\theta >0,\ \forall x,\ f(x|\theta)\leq M_\theta^{n(x)}$,\label{dom_poiss}
\item $\theta\mapsto M_\theta$ est continue,\label{dom_poiss_cont}
\item $\exists C>0, \forall x,\ m(x)\geq C$.\label{stab_model_inf}
\item Toute composante $t(x)_i$ de $t(x)$ est majorée en valeur absolue par une statistique $g(x)$ dont les deux premiers moments existent et sont (majorés par) des fonctions continues des paramètres du modèle.\label{dom_stat}
\end{enumerate}
Les deux premières hypothèses sont des conditions de domination du modèle par un modèle de poisson homogène. La troisième affirme que les valeurs de la variable auxiliaire ne sont pas trop improbables.
\subsubsection{Réalisme des hypothèses}Les hypothèses \ref{dom_poiss} et \ref{dom_poiss_cont} de la proposition \ref{prop_stab_model} sont vérifiées pour le modèle de Strauss et d'interaction par aire.\\
Strauss : Par définition $f(x|\theta)=\beta^{n(x)}\gamma^{s_r(x)}$ pour $\beta>0$ et $\gamma\in[0,1]$. Comme $\gamma^{s_r(x)}\leq 1$, $M=(\beta,\gamma)\mapsto \beta$ convient.\\
Interaction par aire : Ici, $f(x|\theta)=\beta^{n(x)}\eta^{-C_r(x)}$ avec $\beta>0$, $\eta>0$. Comme $0\leq -C_r(x)\leq n(x)$
\begin{equation*}
  M=(\beta,\eta)\mapsto
  \begin{cases}
    \beta\eta&\text{si}\ \eta\geq 1\\
    \beta&\text{sinon}
  \end{cases}
\end{equation*}convient.

L'hypothèse \ref{dom_stat} est vérifiée pour ces deux précédents modèles. En effet
\begin{align}
  0&\leq s_r(x) \leq {n(x) \choose 2}\leq \frac{n(x)^2}{2},
  \intertext{et comme vu plus haut}
  0&\leq -C_r(x)\leq n(x).
  \intertext{Or par convergence dominée, grâce à \ref{dom_poiss}\ref{dom_poiss_cont}}
  \forall p\in\N^*,\ \E[n(x)^p]&=\int n(x)^pp(x|\psi)\mu(dx)
\end{align}
est une fonctionc continue de $\psi$.

\subsection{Ratio des extrema de vraisemblance}
\begin{prop}
  \label{prop_stab_model}
  On fait les hypothèses suivantes : \ref{dom_poiss}, \ref{dom_poiss_cont} et \ref{stab_model_inf}.

Alors
\begin{equation}
  \exists D,D'>0,\ \forall x,\ \frac{M(x)}{m(x)}\leq D'D^{n(x)}.
\end{equation}
\end{prop}
\begin{proof}
  On note $p$ la loi, $f$ la densité non-normalisée et $c$ la constante de normalisation.
  \begin{align}
    \frac{M(x)}{m(x)} &= \frac{\sup_{\theta\in\Theta}p(x|\theta)}{\inf_{\theta\in\Theta}p(x|\theta)}.
    \intertext{Par compacité de $\Theta$ et les hypothèses sur $p(x|\cdot)$ il existe $\theta_{x,s}$ tel que}
    \frac{M(x)}{m(x)} &= \frac{p(x|\theta_{x,s})}{\inf_{\theta\in\Theta}p(x|\theta)}\\
    &=  \frac{f(x|\theta_{x,s})}{c(\theta_{x,s})\inf_{\theta\in\Theta}p(x|\theta)}.
    \intertext{Par les hypothèse \ref{dom_poiss} et \ref{stab_model_inf}}
    \frac{M(x)}{m(x)} &\leq \frac{M_{\theta_{x,s}}^{n(x)}}{C\inf_\theta c(\theta)},
    \intertext{puis par l'hypothèse \ref{dom_poiss_cont}}
    \frac{M(x)}{m(x)} &\leq \frac{\left(\sup_\theta M_\theta\right)^{n(x)}}{C\inf_\theta c(\theta)}.
  \end{align}
  Ce qui donne le résultat voulu.
\end{proof}

\begin{cor}
  \label{cor_stab_model}
  Soit $k\in \N$. Sous les hypothèses de la proposition il existe $C>0$ tel que
  \begin{equation}
    \forall\psi\in\Theta,\ x\sim p(\cdot|\psi)\Rightarrow\E\left[\frac{M(x)^k}{m(x)^k}\right]\leq C.
  \end{equation}
\end{cor}
\begin{proof}
  Par la proposition \ref{prop_stab_model}, il existe $D,D'>0$ tel que 
  \begin{align}
    \left(\frac{M(x)}{m(x)}\right)^k &\leq \left(D'D^{n(x)}\right)^k = D'^k(D^k)^{n(x)}.
    \intertext{On prend l'espérance et on obtient, quitte à changer les notations}
    \E\left[\frac{M(x)^k}{m(x)^k}\right] &\leq \E\left[D'D^{n(x)}\right].
    \intertext{Or, notant $\mu$ la loi du processus de Poisson standard}
    \E\left[D^{n(x)}\right] &= \int D^{n(x)}p(x|\psi)\mu(dx)\\
    &\leq \int D^{n(x)}\frac{M_{\psi}^{n(x)}}{c(\psi)}\mu(dx)\\
    &\leq \frac{1}{\inf_\theta c(\theta)}\int (D\sup_\theta M_\theta)^{n(x)}\mu(dx).
  \end{align}
  D'où le résultat.
\end{proof}

\subsection{Norme $L^2$ du score maximal}
\label{maj_score}
On note $S(x)$ la quantité
\begin{equation}
  \sup_{\theta\in\Theta}\left\|t(x)-\E_\theta [t(X)]\right\|.
\end{equation}
Alors 
\begin{align}
  S(x) &\leq \left\|t(x)-\E t(x)\right\|+\sup_{\theta\in\Theta}\left\|\E t(x)-\E_\theta[t(X)]\right\|\\
  &\leq \left\|t(x)-\E t(x)\right\|+2\sup_{\theta\in\Theta}\left\|\E_\theta[t(X)]\right\|.
  \intertext{Donc, en notant $N_2$ la norme $X\mapsto \sqrt{\E[X^2]}$ sur $L^2$}
  N_2(S) &\leq N_2\left(\left\|t(x)-\E t(x)\right\|\right)+2N_2\left(\sup_{\theta\in\Theta}\left\|\E_\theta[t(X)]\right\|\right)\\
  &\leq \sqrt{\sum_i\Var[t(x)_i]}+2\sup_{\theta\in\Theta}\left\|\E_\theta[t(X)]\right\|.
\end{align}
Par l'hypothèse \ref{dom_stat} et le lemme \ref{lemme_var}, le $\sup$ est fini.
\subsection{Résultat}
Notons $R(x)$ le ratio $\frac{M(x)}{m(x)}$, pour $x$ suivant la loi du modèle.
Les deux sections précédentes permettent de conclure à une majoration de $\E[C_3(x)]$. En effet
\begin{align}
  \E [C_3(x)] &= \E\left[R(x)\left(3+2R(x)\right)\sup_{\theta\in\Theta}\left\|t(x)-\E_\theta [t(X)]\right\|\right]\\
  &= 3\E\left[R(x)S(x)\right]+2\E\left[R(x)^2S(x)\right]
  \intertext{avec la notation de \ref{maj_score}. Ce qui devient avec l'inégalité de Cauchy-Schwarz}
  \E [C_3(x)] &\leq \sqrt{\E[S(x)^2]}\left(3\sqrt{\E[R(x)^2]}+2\sqrt{\E[R(x)^4]}\right).
  \intertext{Par le corollaire \ref{cor_stab_model} le terme entre parenthèses est majoré par une constante $C>0$. Par la section \ref{maj_score} on a donc}
  \E [C_3(x)] &\leq C\left(\sqrt{\sum_i\Var[t(x)_i]}+2\sup_{\theta\in\Theta}\left\|\E_\theta[t(X)]\right\|\right).
\end{align}
Par l'hypothèse \ref{dom_stat} et le lemme \ref{lemme_var} on peut donc majorer $\E[C_3(x)]$ uniformément sur $\Theta$. Car ce dernier est compact.
\subsection{Convergence}
Rappelons \eqref{def_C3}
\begin{equation}
  \label{ecart_noy_unif}
  \sup_{\theta\in\Theta}\left\|P_i^n(\theta,\cdot)-P_s^n(\theta,\cdot)\right\|\leq C_3(x,p,\Theta)n\Delta.
\end{equation}
Les deux noyaux de transition $P_i$ et $P_s$ ci-dessus dépendent de $\Delta$ le pas et $x$ la variable auxiliaire. En notant $E^n(x,\Delta)$ le terme de gauche de \eqref{ecart_noy_unif}, on a donc une constante $C(p,\Theta)>0$ telle que
\begin{equation}
  \label{ecart_noy_moy}
  \E[E^n(x,\Delta)]\leq C(p,\Theta)n\Delta.
\end{equation}
Ce qui implique notamment que $E^n(x,\Delta)\xrightarrow[\Delta\rightarrow 0]{L^1}0$. On peut faire un peu mieux :
\begin{prop}
  Soit $n$ fixé, $\left(\Delta_i\right)_{i\geq 0}$ un suite de pas telle que $\sum_{i\geq 0}\Delta_i<\infty$.
  Alors,
  \begin{equation}
    \forall x,\ E^n(x,\Delta_i)\cvps[i]0.
  \end{equation}
\end{prop}
\begin{proof}
  Par l'inégalité de Markov et \eqref{ecart_noy_moy} on a pour tout $\varepsilon >0$
  \begin{equation}
    \P(E^n(x,\Delta_i)\geq \varepsilon) \leq \frac{C(p,\Theta)n\Delta_i}{\varepsilon}.
  \end{equation}
  Alors, comme $\sum_{i\geq 0}\Delta_i<\infty$ par hypothèse, le lemme de Borel-Cantelli implique le résultat voulu.
\end{proof}
\appendix
\section{Annexe}
\begin{lemme}
  \label{lemme_var}
  Soit $X,Y$ deux variables aléatoires réelles possédant une variance telles que $\left|X\right|\leq Y$. Alors
  \begin{equation*}
    \Var[X] \leq \Var[Y] + \E[Y]^2
  \end{equation*}
\end{lemme}
\begin{proof}
  \begin{align}
    \Var[X] &= \E[X^2]-\E[X]^2\\
    &\leq \E[Y^2]-\E[X]^2\\
    &\leq \E[Y^2]+\E[Y]^2-\E[Y]^2-\E[X]^2\\
    &\leq \Var[Y]+\E[Y]^2-\E[X]^2\\
    &\leq \Var[Y]+\E[Y]^2.
  \end{align}
\end{proof}
\bibliographystyle{plain}
\bibliography{../biblio}
\end{document}

