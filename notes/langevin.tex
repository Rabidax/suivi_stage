\documentclass[10pt, a4paper]{article}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{hyperref}
\usepackage{graphicx}
%% \usepackage[left=2cm,right=2cm]{geometry}

\input{../commandes_perso}

\title{ABC Shadow et diffusion de Langevin}
\author{Jean-Léon \bsc{Henry}}

\begin{document}
\maketitle
On note $(\Omega, \mathcal{T}, \P)$ l'espace de probabilité sur lequel sont définies nos variables aléatoires.
\section{Diffusion de Langevin}
Soit l'EDS
\begin{equation}
  dX_t = \frac{1}{2}\nabla\log f(X_t)dt+dB_t\label{eds_langevin}
\end{equation}
où $f$ est une densité de probabilité strictement positive partout et $C^1$, $X_t$ un processus à valeurs dans $\R^d$ et $B_t$ le mouvement brownien.

Sous une hypothèse de croissance polynomiale du terme de dérive, \cite[Théorème 2.1]{roberts1996} donne
\begin{thm}
  \begin{itemize}
  \item f est invariante pour la diffusion de Langevin
  \item $\forall x,\ \left\|P^t(x,\cdot)-f\right\|_{TV}\xrightarrow{t\rightarrow\infty} 0$,
  \end{itemize}
où $P^t(x,A)=\P(X_t\in A|X_0=x).$
\end{thm}
En discrétisant cette équation on peut espérer obtenir un schéma d'échantillonage de la loi $f$ :
\begin{equation}
  X_{n+1} = X_n + \frac{\gamma}{2}\nabla\log f(X_n)+\sqrt{\gamma}\epsilon_n\label{sgld}
\end{equation}
où les $\epsilon_n\sim\mathcal{N}(0,I_d)$ sont indépendants, et $\gamma$ est notre discrétisation du temps.

\cite{roberts1996} explique que cette discrétisation naïve ne garde pas les bonnes propriétés de convergence de son alter-ego continue. Une solution proposée est d'utiliser \eqref{sgld} comme proposition dans un algorithme de MH. C'est-à-dire qu'on utilise la loi instrumentale suivante
\begin{equation}
  q(x\rightarrow \cdot) \sim \mathcal{N}(x+\frac{\gamma}{2}\nabla\log f(x),\gamma I_d)
\end{equation}
Cela donne une probabilité d'acceptation de la forme :
\begin{equation}
  \alpha(x\rightarrow y) = 1\wedge\frac{f(y)}{f(x)}\frac{\exp(-\left\|x-y-\gamma/2\nabla\log f(y)\right\|^2/(2\gamma))}{\exp(-\left\|y-x-\gamma/2\nabla\log f(x)\right\|^2/(2\gamma))}.\label{proba_accept}
\end{equation}
Cette fois le noyau de transition de la chaîne converge bien vers $f$ en TV, comme dans le cas continu, mais pour $f$-presque toutes les conditions initiales.

Une étape de raffinement supplémentaire consiste à utiliser un estimateur sans biais du gradient, c'est l'algorithme de Langevin-Gradient Stochastique proposé par \cite{WellingTeh2011}

\section{Application à l'inférence bayésienne}
Maintenant prenons $f$ de la forme suivante
\begin{equation}
  f\coloneqq \theta\longmapsto f(\theta|y) = \frac{e^{\theta\cdot t(y)}p(\theta)}{c(\theta)Z(y)}.
\end{equation}
C'est-à-dire la postérieure d'un processus ponctuels de Gibbs, en ce qui nous concerne. On a noté $y$ les observations. Alors
\begin{align}
  \nabla \log f(\theta|y) &= \nabla\log\left(\frac{e^{\theta\cdot t(y)}p(\theta)}{c(\theta)Z(y)}\right)\\
  \intertext{ce qui donnne, si on prend une loi uniforme pour l'a priori}
  &= \nabla(\theta\cdot t(y)-\log c(\theta))\\
  &= t(y)-\nabla\log c(\theta)\\
  &= t(y) - \E_\theta(t(X))
\end{align}
où $X\sim f(\cdot|\theta)$. \eqref{proba_accept} devient
\begin{equation}
  \alpha(\theta\rightarrow \psi) = 1\wedge\frac{f(\psi|y)}{f(\theta|y)}\frac{\exp(-\left\|\theta-\psi-\gamma/2\left(t(y) - \E_\psi t(X)\right)\right\|^2/(2\gamma))}{\exp(-\left\|\psi-\theta-\gamma/2\left(t(y) - \E_\theta t(X)\right)\right\|^2/(2\gamma))}.\label{proba_accept_appli}
\end{equation}

\section{Lien avec ABC Shadow}
Pour rappel, une itération d'ABC Shadow se passe ainsi. Si le système est dans l'état $\theta$
\begin{itemize}
\item On propose un nouvel état $\psi$ simulé selon la loi uniforme sur la boule $B(\theta, \Delta/2)$.
\item On accepte cet état avec le taux $\alpha_s$ définit par :
  \begin{equation}
    \alpha_s(\theta\rightarrow\psi) = 1\wedge\frac{f(\psi|y)}{f(\theta|y)}\cdot\frac{h(x|\theta)c(\psi)\indic_{B(\psi, \Delta/2)}(\theta)}{h(x|\psi)c(\theta)\indic_{B(\theta, \Delta/2)}(\psi)}
  \end{equation} où $y$ sont les données et $x$ est la variable auxiliaire.
\end{itemize}
Mais on peut réécrire ce taux autrement. Tout d'abord comme $\psi$ est choisi dans la boule $B(\theta, \Delta/2)$ les indicatrices valent toutes les deux 1. Et alors
\begin{align}
  \alpha_s(\theta\rightarrow\psi)&=1\wedge\frac{h(\psi|y)}{h(\theta|y)}\cdot\frac{h(x|\theta)c(\psi)}{h(x|\psi)c(\theta)}\\
  &= 1\wedge \frac{h(y|\psi)h(x|\theta)}{h(y|\theta)h(x|\psi)}\\
  &= 1\wedge e^{(t(y)-t(x))\cdot(\psi-\theta)}
  \intertext{Le terme en $t(y)-t(x)$ est présent dans Robbins-Monro appliqué à notre cas. Grâce à $\forall u,v\in\R^d,\ u\cdot v=\frac{\left\|u+v\right\|^2-\left\|u-v\right\|^2}{4}$, on en peut continuer}
  &= 1\wedge \frac{\exp\left(-\frac{1}{4}\left\|\psi-\theta-(t(y)-t(x))\right\|^2\right)}{\exp\left(-\frac{1}{4}\left\|\psi-\theta+(t(y)-t(x))\right\|^2\right)}\\
  &= 1\wedge \frac{\exp\left(-\frac{1}{4}\left\|\theta-\psi-(t(x)-t(y))\right\|^2\right)}{\exp\left(-\frac{1}{4}\left\|\psi-\theta-(t(x)-t(y))\right\|^2\right)}
\end{align}
On retrouve presque \eqref{proba_accept_appli} dans le cas particulier $\gamma=2$.

\bibliographystyle{plain}
\bibliography{../biblio}
\end{document}
