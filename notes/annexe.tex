
\section{Martingales}
\subsection{Définitions}
Une suite $(X_n)$ de variables aléatoires sur un espace de probabilité $(\Omega, \mathcal{F}, \mathbb{P})$, adaptée à une filtration $\mathcal{F}_n$ de $\mathcal{F}$ est une martingale si
\begin{enumerate}
\item $(X_n)$ est intégrable
\item $\forall n\geq 0,\ \E_nX_{n+1}=X_n$.
\end{enumerate}
$(X_n)$ est une sur-martingale si
\begin{enumerate}
\item $(X_n^-)$ est intégrable
\item $\forall n\geq 0,\ \E_nX_{n+1}\leq X_n$.
\end{enumerate}
$(X_n)$ est une sous-martingale si $(-X_n)$ est une surmartingale.

\paragraph{Exemple}Soit une marche aléatoire définie sur $\mathbb{Z}$ par une valeur initiale $X_0$ quelconque et pour tout $n\geq 1$
\begin{equation*}
  X_{n}=X_0+B_1+\ldots+B_n
\end{equation*}
où $(B_i)_i$ est une suite i.i.d. de variables de Bernoulli de loi $\P(B=-1)=\P(B=1)=1/2$. On choisit la filtration $\mathcal{F}_n=\sigma(X_0,\ldots,X_n)$ et on a alors
\begin{equation*}
  \E_n(X_{n+1})-X_n=\E_n(X_{n+1}-X_n)=\E_n(B_{n+1})=\E(B_{n+1})=0
\end{equation*}
Donc ce processus est une martingale.
\subsection{Propriétés}
Nous donnons maintenant quelques propriétés des martingales, qui sont toutes des conséquences directes de la définition.

Tout d'abord, une propriété de monotonie des surmartingales. Le résultat s'adapte facilement aux sousmartingales et aux martingales.
\begin{prop}
  Soit $(X_n)$ une surmartingale. Alors $(\E X_n)$ est une suite décroissante.
\end{prop}
Ensuite, un moyen facile de créer une sousmartingale.
\begin{prop}
  Soit $(X_n)$ une martingale et $\phi$ une fonction convexe telle que pour tout entier $n$, $\E|\phi(X_n)|<\infty$. Alors $(\phi(X_n))$ est une sousmartingale.
\end{prop}
Un exemple fécond étant le cas $\phi(x)=x^2$.
\subsection{Convergence}
\begin{thm}[Doob]
  \label{th_doob}
  Soit $(X_n)$ une martingale bornée dans $L^1$ (i.e. $\sup_n\E|X_n|<\infty$). Alors il existe $X_\infty\in L^1$ tel que $X_n\cvps X_\infty$.
\end{thm}
Le théorème reste vrai pour une surmartingale ou une sousmartingale. En particulier :
\begin{cor}
  Soit $(X_n)$ une surmartingale minorée. Alors il existe $X_\infty$ vers laquelle $X_n$ converge presque sûrement.
\end{cor}
\begin{proof}
  Notons $(X_n)$ la surmartingale, et $c$ une constante telle que $\forall n,\ X_n\geq c$. Comme $X-c$ est encore une surmartingale on peut supposer $c=0$, quitte à changer les notations, i.e. $X_n$ positive. Alors
  \begin{equation}
    \E|X_n|=\E X_n\leq\E X_0
  \end{equation}
  et le théorème \ref{th_doob} s'applique.
\end{proof}
%% Théorème d'arrêt
Le résultat qui suit permet, si on choisit bien un temps d'arrêt, d'appliquer le théorème de Doob grâce une troncature.
\begin{thm}[Théorème d'arrêt]
  Soit $(X_n)$ une martingale, sous-martingale ou surmartingale et soit T un temps d'arrêt adapté à $(\mathcal{F}_n)$. Alors, $(X_{n\wedge T})$ est également une martingale, sousmartingale ou surmartingale, respectivement.
\end{thm}
%% Martingales de carré intégrable
%% TLC
\begin{thm}[Théorème de limite centrale]
  Soit $M_n$ une martingale de carré intégrable, $\left<M\right>_n$ son crochet et $a_n$ un suite déterministe croissant vers $\infty$. Si
  \begin{enumerate}
  \item $\exists \Gamma\geq 0,\ a_n^{-1}\left<M\right>_n\cvproba \Gamma$,
  \item $\forall \epsilon>0,\ a_n^{-1}\sum_{k=1}^n\E_{k-1}\left((M_k-M_{k-1})^2\indic_{|M_k-M_{k-1}|\geq\epsilon \sqrt{a_n}}\right)\cvproba 0$,
  \end{enumerate}
  alors $\frac{1}{\sqrt{a_n}}M_n\cvlaw\mathcal{N}(0,\Gamma)$.
\end{thm}
\begin{proof}
  Voir \cite{bercuetal} pour une preuve dans un cadre restreint et des indications bibliograhiques.
\end{proof}

\section{Preuves}
\begin{proof}[Démonstration du lemme \ref{RS_cor}]
  Il suffit d'utiliser le lemme suivant
  \begin{lemme}(Robbins-Sigmund)
  \label{RS}
  Soit $(V_n)$,$(A_n)$,$(B_n)$ trois suites positives adaptées à la filtration. On suppose $V_0$ intégrable  et
  \begin{equation}
    \label{hyp_recc_RS}
    \forall n\geq0,\ \E_nV_{n+1}\leq V_n+A_n-B_n.
  \end{equation}
  Alors si
  \begin{equation*}
    \sum_{n\geq0}A_n<\infty\text{ p.s.}
  \end{equation*}
  $V_n$ converge p.s. vers $V_\infty$ finie p.s. et
  \begin{equation*}
    \sum_{n\geq0}B_n<\infty\text{ p.s.}
  \end{equation*}
  \end{lemme}
  \begin{proof}[Démonstration du lemme \ref{RS}]
      Si $\forall n,\ A_n=B_n=0$, alors $V_n$ est une surmartingale positive donc converge p.s. vers $V_\infty$ finie p.s. et le reste de l'énoncé est trivial.
  Dans le cas général, on définit
  \begin{equation*}
    \forall n\geq0,\ M_n \coloneqq V_n - \sum_{k=0}^{n-1}(A_k-B_k)
  \end{equation*}
  avec la convention $M_0 = V_0$.
  $M_n$ est une surmartingale, en effet pour $n\geq0$
  \begin{align}
    M_{n+1} &= V_{n+1} - \sum_{k=0}^{n}(A_k-B_k)\\
    &= V_{n+1} - V_n + (V_n - \sum_{k=0}^{n-1}(A_k-B_k))-(A_n-B_n)\\
    &= M_n + V_{n+1} - V_n -(A_n-B_n).
  \end{align}
  Ce qui par l'hypothèse \ref{hyp_recc_RS} implique bien
  \begin{equation}
    \E_nM_{n+1} \leq M_n.
  \end{equation}
  Maintenant, considérons pour $a>0$ le temps d'arrêt suivant
  \begin{equation*}
    T_a \coloneqq \inf\left\{n\geq0;\sum_{k=0}^n(A_k-B_k)>a    \right\}.
  \end{equation*}
  Par le théorème d'arrêt, $M_{n\wedge T_a}$ est une surmartingale. Elle est de plus minorée :
  \begin{equation}
          \forall n\leq T_a,\ M_n \geq -\sum_{k=0}^{n-1}(A_k-B_k)\geq -a
  \end{equation}
  par définition de $T_a$. Donc elle converge p.s. vers $M_\infty$ finie p.s.
  Donc on a convergence $M_n\xrightarrow{\text{p.s.}}M_\infty$ sur $\left\{T_a=\infty\right\}$.

  Quant à la convergence de $\sum_n B_n$, on a :
  \begin{equation}
    M_{n+1}+\sum_{0\leq k\leq n}A_k\geq \sum_{0\leq k\leq n}B_k\geq 0.
  \end{equation}
  Le terme de gauche converge p.s. sur $\left\{T_a=\infty\right\}\cap\Gamma$ où on note $\Gamma$ l'ensemble $\left\{\sum_kA_k<\infty\right\}$. Donc le lemme est vrai sur cet ensemble, quelque soit $a>0$.

  Sur $\Gamma$,
  \begin{equation}
    \sum_{k=0}^n(A_k-B_k) \leq \sum_{k=0}^nA_k\leq\sum_{k=0}^\infty A_k<\infty.
  \end{equation}
  Donc
  \begin{equation}
    \exists p\in\N^*,\ \forall n,\ \sum_{k=0}^n(A_k-B_k)\leq p,
  \end{equation}
  i.e.
  \begin{equation}
    \exists p\geq 1,\ T_p=\infty.
  \end{equation}
  Donc $\Gamma\subset\cup_{p\geq 1}\{T_p=\infty\}$, d'où $\Gamma = \cup_{p\geq 1}\Gamma\cap\{T_p=\infty\}$, ce qui conclut la preuve.
  \end{proof}
    On se ramène au lemme \ref{RS} en posant
  \begin{align}
    \alpha_n &= \prod_{k=1}^n\frac{1}{1+a_k}\\
    V_n' &= \alpha_{n-1}V_n\\
    A_n' &= \alpha_{n}A_n\\
    B_n' &= \alpha_{n}B_n.\\
    \intertext{Car en effet de}
    \E_nV_{n+1}&\leq V_n(1+a_n)+A_n-B_n
    \intertext{on obtient, en multipliant par $\alpha_n$,}
    \E_nV'_{n+1}&\leq V'_n+A'_n-B'_n.
  \end{align}
  Comme $\alpha_n\leq 1$, $0\leq A_n'\leq A_n$. Donc $\sum_{n\geq 0} A_n'<\infty$ et le lemme \ref{RS} s'applique. C'est-à-dire $V_n'\cvps V_\infty'$ et $\sum B_n'<\infty$.

  Par définition on a que
  \begin{align}
    \log(\alpha_{n-1}) &= -\sum_{k=1}^{n-1}\log(1+a_k).
    \intertext{En remarquant que}
    0&\leq \log(1+a_k)\leq a_k\label{cv_alpha},
    \intertext{l'hypothèse sur $\sum_{n\geq 0} a_n$ donne}
    \alpha_n &\cvps \alpha_\infty.
    \intertext{Et donc $V_n$ converge p.s. Mais \eqref{cv_alpha} montre aussi que $1/\alpha_n$ converge p.s., donc est bornée p.s. On en déduit qu'il existe $M>0$ tel que}
    0&\leq B_n\leq MB_n'\text{ p.s.}
  \end{align}
  Et donc $\sum_{n\geq 0} B_n<\infty$ p.s. ce qui termine la preuve.
\end{proof}
