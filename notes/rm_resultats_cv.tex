%%
%% CV P.S.
%%

\subsection{Convergence presque sûre}
Tout d'abord il convient de se demander si on a bien convergence de la suite des $X_n$ vers l'optimum. On fait pour cela les hypothèses suivantes: 
\paragraph{Hypothèses}
\begin{enumerate}[(H1)]
  \item $\phi$ continue \label{hyp_ps_1}
  \item $\forall x\neq x^*,\ (x-x^*)(\phi(x)-\alpha)<0$\label{hyp_ps_2}
  \item $\forall n\geq0,\ \E_nY_{n+1}=\phi(X_n)$\label{hyp_ps_3}
  \item $\exists C>0,\ \forall n\geq0,\ \E_nY_{n+1}^2 \leq C(1+X_n^2)    $\label{hyp_ps_moment}
  \item $\gamma_n$ décroît vers zéro, $\sum_{n\geq 0}\gamma_n = \infty$, $\sum_{n\geq 0}\gamma_n^2<\infty$\label{hyp_ps_end}
\end{enumerate}

Discutons rapidement ces hypothèses. La première est une hypothèse de régularité, la deuxième donne l'unicité de la solution. Après ces hypothèses sur $\phi$, les deux hypothèses suivantes concernent le lien entre les $Y_n$ et les $X_n$. Les $Y_n$ doivent approcher en moyenne les valeurs de $\phi$ et on veut pouvoir maîtriser leur variance. La dernière hypothèse impose la vitesse de convergence du pas vers zéro : suffisamment lentement pour être sur de s'approcher de l'optimum et assez vite pour y converger.
\begin{thm}\label{th_cvps}
  Sous les hypothèses \ref{hyp_ps_1}-\ref{hyp_ps_end}, la suite $(X_n)_{n\geq0}$ converge p.s. vers $x^*$.
\end{thm}
On commence par énoncer un lemme crucial. Nous démontrons ensuite le théorème. La démonstration du lemme est donnée en annexe.

\begin{lemme}
  \label{RS_cor}
    Soit $(V_n)$,$(A_n)$,$(B_n)$,$(a_n)$ quatres suites positives adaptées à la filtration $(\mathcal{F}_n)$. On suppose $V_0$ intégrable  et
  \begin{equation*}
    \forall n\geq0,\ \E_nV_{n+1}\leq V_n(1+a_n)+A_n-B_n.
  \end{equation*}
  Alors si
  \begin{equation*}
    \sum_{n\geq0}a_n<\infty\text{ et }\sum_{n\geq0}A_n<\infty\text{ p.s.}
  \end{equation*}
  $V_n$ converge p.s. vers $V_\infty$ finie p.s. et
  \begin{equation*}
    \sum_{n\geq0}B_n<\infty\text{ p.s.}
  \end{equation*}
\end{lemme}


\begin{proof}[Démonstration du théorème \ref{th_cvps}]
  On définit $\forall n\geq0,\ V_n\coloneqq(X_n-x^*)^2$ et on va prouver que $V_n\cvps 0$.

  On a pour $n\geq0$
  \begin{align}
    V_{n+1} &= (X_{n+1}-x^*)^2\\
    &= (X_n-x^*+\gamma_n(Y_{n+1}-\alpha))^2\\
    &= V_n + 2\gamma_n(X_n-x^*)(Y_{n+1}-\alpha)+\gamma_n^2(Y_{n+1}-\alpha)^2.
  \end{align}
  Par l'hypothèse \ref{hyp_ps_3}  on a
  \begin{equation}
    \label{decomp_carre}
        \E_nV_{n+1} = V_n + 2\gamma_n(X_n-x^*)(\phi(X_n)-\alpha)+\gamma_n^2\E_n\left((Y_{n+1}-\alpha)^2\right).
  \end{equation}
  Estimons maintenant le dernier terme. Comme pour tout réels $a,b$, $(a+b)^2\leq 2a^2+2b^2$,
  \begin{align}
    \E_n\left((Y_{n+1}-\alpha)^2\right) &\leq \E_n\left(2Y_{n+1}^2+2\alpha^2\right).\\
    \intertext{En utilisant l'hypothèse \ref{hyp_ps_moment}, et comme $\alpha$ est une constante}
    \E_n\left((Y_{n+1}-\alpha)^2\right) &\leq 2C(1+X_n^2)+2\alpha^2.\\
    \intertext{Puis en faisant rentrer le terme en $\alpha$ dans la constante on déduit que}
    \E_n\left((Y_{n+1}-\alpha)^2\right) &\leq C'(1+X_n^2)\leq C''(1+V_n)
  \end{align}
  avec $C''>0$. La dernière inégalité vient du fait que $x\mapsto \frac{1+x^2}{1+(x-b)^2}$ est bornée sur $\R$ quelque soit $b$ : en effet, cette fonction est continue sur $\R$ et tend vers 1 à l'infini. L'équation \eqref{decomp_carre} donne alors
  \begin{equation}
    \label{decomp_carre_final}
    \E_nV_{n+1} \leq V_n(1 + C''\gamma_n^2) + C''\gamma_n^2 + 2\gamma_n(X_n-x^*)(\phi(X_n)-\alpha).
  \end{equation}
  On peut appliquer le lemme \ref{RS_cor} grâce aux hypothèses \ref{hyp_ps_2} et \ref{hyp_ps_end} : $V_n$ converge p.s. vers $V_\infty$ finie p.s. et
  \begin{equation}
    \sum_n 2\gamma_n(X_n-x^*)(\phi(X_n)-\alpha)<\infty\text{ p.s.}\label{cv_B}
  \end{equation}
  Montrons maintenant que $V_\infty=0$ p.s.

  Soit $\omega\in\left\{V_n\rightarrow V_\infty\right\}\cap\left\{\text{\eqref{cv_B} a lieu}\right\}$.
  Si $V_\infty(\omega)\neq0$, ou de manière équivalente $V_\infty(\omega)>0$, on peut trouver deux réels $a$ et $b$ dépendant de $V_\infty(\omega)$ tels que 
  \begin{equation}
    \exists N\in\N,\ \forall n\geq N,\ X_n(\omega)\in\left\{y\in\R;0<a\leq|y-x^*|\leq b\right\}.
  \end{equation}
  Comme $V_\infty(\omega)>0$, l'hypothèse \ref{hyp_ps_2}
  implique bien $a>0$. Sur cet anneau compact, la fonction continue
  $x\mapsto (x-x^*)(\phi(x)-\alpha)$ est donc majorée par un réel $c<0$. Ce qui implique par \eqref{cv_B} que $\sum_n\gamma_n$ converge. Cette contradiction implique $V_\infty(\omega)=0$.

  Le raisonnement de ce précédent paragraphe est vrai quelque soit $\omega\in\left\{V_n\rightarrow V_\infty\right\}\cap\left\{\text{\eqref{cv_B} a lieu}\right\}$ qui est un évènement certain. Donc $V_\infty=0$ p.s.
\end{proof}

%%
%% NORMALITE ASYMP
%%

\subsection{Théorème de la limite centrale}
Il convient ensuite d'estimer la qualité de l'estimation de l'optimum fournie par le théorème précédent. On fait donc les hypothèses suivantes : 
\paragraph{Hypothèses supplémentaires}
\begin{enumerate}[(H1), resume]
\item $\phi\in\mathcal{C}^1$
\item $\exists p>2,\ \sup_n\E_n\left(|Y_{n+1}-\phi(X_n)|^p\right)<\infty  $\label{hyp_tlc_lindenberg}
\item $\sigma^2\coloneqq\lim_n\left(\E_nY_{n+1}^2-\phi(X_n)^2\right)$ existe\label{hyp_tlc_end}
\end{enumerate}
La première hypothèse donne une régularité supplémentaire de $\phi$ ce qui va nous permettre d'estimer le comportement de la suite $X_n$ à l'ordre suivant. Comme vu plus haut, les martingales jouent un rôle important ici, d'où les deux autres hypothèses supplémentaires. Elles permettent d'appliquer un théorème centrale limite pour les martingales qui est donné en annexe.

On note $q = -\phi'(x^*)\geq 0$.

\paragraph{Notations} Les symboles `"p.s.", "$\mathcal{L}$", "$L^1$" et "$\mathbb{P}$" au dessus d'une flèche de convergence indiquent respectivement que cette convergence a lieu presque sûrement, en loi, en moyenne ou en probabilité.
\begin{thm}
Sous les hypotèses \ref{hyp_ps_1}-\ref{hyp_tlc_end}, avec le pas $\gamma_n=1/n$, on décompose le résultat selon la valeur de $q$ :
\begin{enumerate}
\item Si $q>1/2$ alors \[\sqrt{n}\left(X_n-x^*\right)\cvlaw\mathcal{N}(0,\sigma^2/(2q-1)).\]
\item Si $q=1/2$ alors \[\sqrt{\frac{n}{\log(n)}}\left(X_n-x^*\right)\cvlaw\mathcal{N}(0,\sigma^2).\]
\item Si $q<1/2$ alors \[n^q\left(X_n-x^*\right)\cvps\text{une variable aléatoire finie}.\]
\end{enumerate}
\end{thm}

\begin{proof}
La preuve se fait en plusieurs étapes. On se restreint à $q>1/2$ pour simplifier une preuve déjà longue, mais aussi pour des raisons mentionnées plus loin.
\begin{enumerate}
\item Preuve de la décomposition \[\forall n>1,\ X_n-x^* = \underbrace{\beta_{n-1}(X_0-x^*)}_{\textcircled{1}}+\underbrace{\beta_{n-1}M_n}_{\textcircled{2}}+\underbrace{\beta_{n-1}R_{n-1}}_{\textcircled{3}}\] où $M_n$ est une martingale et $R_n$ un reste aléatoire.
\item Recherche d'un équivalent de $\beta_n$
\item Convergence du terme \textcircled{1}
\item Convergence du terme \textcircled{2}
\item Convergence du reste \textcircled{3}
\end{enumerate}
\paragraph{Etape 1}Etudions la convergence de $X_n-x^*$. Soit $n\geq 1$.
\begin{align}
X_n - x^* &= \left(X_{n-1}-x^* + \gamma_{n-1}(Y_n-\alpha)\right)\\
&= \left(X_{n-1}-x^*\right) + \gamma_{n-1}(Y_n-\phi(X_{n-1}))+\gamma_{n-1}(\phi(X_{n-1})-\alpha).
\intertext{On définit $\delta_n$ par l'équation suivante (approximation d'ordre 1)}
&\forall n,\ \phi(X_n) = \alpha-q(X_n-x^*)+\delta_n.\\
\intertext{On a alors}
X_n-x^* &= (1-q\gamma_{n-1})(X_{n-1}-x^*) + \gamma_{n-1}(Y_n-\phi(X_{n-1}))+\gamma_{n-1}\delta_{n-1},\\
\intertext{ce qui donne par récurrence}
\forall n\geq1,\ &X_n-x^* = \beta_{n-1}(X_0-x^*)+\beta_{n-1}M_n+\beta_{n-1}R_{n-1}\label{decomp}
\end{align}
avec
\begin{align}
\beta_{n-1} &= \prod_{k=0}^{n-1}\left(1-q\gamma_k\right)\\
M_n &= \sum_{k=0}^{n-1}\frac{\gamma_k}{\beta_k}Z_{k+1}\\
Z_{k+1} &= Y_{k+1}- \phi(X_{k})\\
R_{n-1} &= \sum_{k=0}^{n-1}\frac{\gamma_k}{\beta_k}\delta_{k}.\label{def_reste}
\end{align}
Ici $M_n$ est une martingale car par hypothèse $\forall n,\ \E_nZ_{n+1}=0$. On note son crochet dès à présent, il nous servira plus loin :
\begin{equation}
\left<M\right>_n = \sum_{k=0}^{n-1}\left(\frac{\gamma_k}{\beta_k}\right)^2\E_kZ_{k+1}^2.\label{crochet}
\end{equation}
\paragraph{Etape 2}
Nous allons montrer que
\begin{equation}
\exists c>0, \beta_n \sim ce^{-qs_n}\label{equiv_beta}
\end{equation}
où $s_n = \sum_{0\leq k\leq n}\gamma_k$. Si $q=0$, \eqref{equiv_beta} est trivial. Soit $0<x<1$. Alors
\begin{equation*}
\exists y\in]0,x[,\ 0\leq\log(1-x)+x = \frac{x^2}{2(1-y)^2}.
\end{equation*}
Pour $n$ assez grand $0 <q\gamma_n < 1$ et comme la suite $\gamma_n$ décroît
\begin{equation}
\exists n_0,\ \forall n\geq n_0,\ \log(1-q\gamma_n)+q\gamma_n \leq \frac{q^2\gamma_n^2}{2(1-q\gamma_{n_0})^2}.
\end{equation}
Donc par l'hypothèse \ref{hyp_ps_end}, le terme de gauche de l'équation ci-dessus est une suite positive majorée par le terme général d'une série convergente. On peut donc conclure en passant à l'exponentielle.

Dans ce qui suit on prend $\gamma_n = \gamma/n$, $\gamma>0$. L'énoncé correspond au cas $\gamma=1$. En particulier,
\begin{equation}
\beta_k \sim ce^{-q\gamma(\log(k)+C_{e}+o(1))} \sim ck^{-q\gamma}
\end{equation}
où $C_e>0$ est la constante d'Euler qu'on assimilera par la suite dans la constante.
\paragraph{Etape 3}Le premier terme est facile à manipuler. En effet
\begin{equation}
\sqrt{n}\beta_{n-1}(X_0-x^*) = \mathcal{O}(n^{1/2-q\gamma})
\end{equation}
ce qui tend presque sûrement vers zéro sous l'hypothèse $q\gamma>1/2$.
\paragraph{Etape 4}Cherchons un équivalent du crochet \eqref{crochet}. L'étape 2 et l'hypothèse \ref{hyp_tlc_end} donnent
\begin{align}
\left(\frac{\gamma_k}{\beta_k}\right)^2\E_kZ_{k+1}^2 &\sim \sigma^2\gamma^2c^{-2}k^{2q\gamma-2}.
\intertext{Si on suppose $2q\gamma-1> 0$, on peut sommer les équivalents et on a}
\left<M\right>_k &\sim \sigma^2\gamma^2c^{-2}\frac{k^{2q\gamma-1}}{2q\gamma-1}.
\intertext{En notant $a_k = c^{-2}k^{2q\gamma-1}$, on a aussi}
a_k^{-1}\left<M\right>_k &\xrightarrow[k\rightarrow\infty]{} \frac{\sigma^2\gamma^2}{2q\gamma-1}.
\end{align}
Il reste à vérifier la condition de Lindenberg, i.e.
\begin{equation}
\forall \epsilon>0,\ a_n^{-1}\sum_{k=1}^n\E_{k-1}\left((M_k-M_{k-1})^2\indic_{|M_k-M_{k-1}|\geq\epsilon \sqrt{a_n}}\right)\cvproba 0.\label{lindenberg}
\end{equation}
Explicitons cette somme dans notre cas :
\begin{align}
  \sum_{k=1}^n\E_{k-1}\left((M_k-M_{k-1})^2\indic_{|M_k-M_{k-1}|\geq\epsilon \sqrt{a_n}}\right)&=\sum_{k=1}^n\E_{k-1}\left[\left(\frac{\gamma_k}{\beta_k}Z_k\right)^2\indic_{\left|\frac{\gamma_k}{\beta_k}Z_k\right|\geq\epsilon \sqrt{a_n}}\right]\\
  &= \sum_{k=1}^n\left(\frac{\gamma_k}{\beta_k}\right)^2\E_{k-1}\left[Z_k^2\indic_{\left|Z_k\right|\geq\left|\frac{\beta_k}{\gamma_k}\right|\epsilon \sqrt{a_n}}\right].
\end{align}
Pour vérifier la condition \eqref{lindenberg}, soit $\epsilon,b >0$. Par l'hypothèse \ref{hyp_tlc_lindenberg}
\begin{align}
\E_{k-1}\left(Z_k^2\indic_{|Z_k|\geq b}\right)\leq\frac{\E_{k-1}\left(|Z_k|^p\right)}{b^{p-2}}&\leq \frac{L}{b^{p-2}}
\intertext{où $L$ est une variable aléatoire finie p.s. indépendante de $k$. Donc, avec $b=\epsilon \sqrt{a_n}|\frac{\beta_k}{\gamma_k}|$}
a_n^{-1}\sum_{k=1}^n\E_{k-1}\left((M_k-M_{k-1})^2\indic_{|M_k-M_{k-1}|\geq\epsilon \sqrt{a_n}}\right)&\leq \frac{L'}{a_n^{(p-2)/2+1}}\sum_{k=1}^n\left|\frac{\gamma_k}{\beta_k}\right|^p\label{lindenberg_estim}
\end{align}
avec $L'=L/\epsilon^{p-2}$. Comme on sait que $\left|\frac{\gamma_k}{\beta_k}\right|^p \sim \gamma^pc^{-p}k^{p(q\gamma-1)}$, deux cas se présentent :
\begin{enumerate}
\item Soit $p(q\gamma-1)<-1$, %% CV
\item Soit $p(q\gamma-1)\geq -1$ %% DV
\end{enumerate}
\subparagraph{Cas 1}Si $p(q\gamma-1)<-1$ la somme dans le terme de droite dans \eqref{lindenberg_estim} converge. Ce terme est donc en
\begin{equation}
  \bigo\left(\frac{1}{a_n^{1+(p-2)/2}  }  \right)
\end{equation}
qui converge vers zéro quand $n$ tend vers l'infini.
\subparagraph{Cas 2}Si $p(q\gamma-1)>-1$ on a cette fois un équivalent de la somme, et donc l'estimation \eqref{lindenberg_estim} est en
\begin{equation}
  \bigo\left(\frac{n^{p(q\gamma-1)+1}  }{n^{(2q\gamma-1)(1+(p-2)/2)}  }
  \right).
\end{equation}
Un calcul rapide donne $p(q\gamma-1)+1-(2q\gamma-1)p/2=1-p/2$. Ce qui est strictement négatif par hypothèse sur $p$. Le cas $p(q\gamma-1)=-1$ compare simplement $\log(n)$ à une puissance positive de $n$. Et donc la condition de Lindenberg est aussi bien vérifiée dans ce cas.

Dans les deux cas, on peut donc appliquer le théorème de limite centrale pour les martingales donné en annexe et on a
\begin{align}
a_n^{-1/2}M_n&\cvlaw\mathcal{N}(0,\sigma^2\gamma^2/(2q\gamma-1)).
\intertext{On peut maintenant traiter le terme en $M_n$ dans \eqref{decomp} en remarquant que}
a_n^{-1/2} &\sim \sqrt{n}\beta_{n-1}.
\intertext{Donc finalement, on a que}
\sqrt{n}\beta_{n-1}M_n&\cvlaw\mathcal{N}(0,\sigma^2\gamma^2/(2q\gamma-1)).
\end{align}
\paragraph{Etape 5}Montrons que
\begin{equation}
\sqrt{n}\beta_{n}R_{n}\cvmoy 0
\end{equation}
où $R_n$ est définit en \eqref{def_reste}. On se donne $\epsilon>0$ et on sépare $\E(|\sqrt{n}\beta_{n}R_{n}|)$ en deux termes : 
\begin{align}
\E(|\sqrt{n}\beta_{n}R_{n}|)&=  \sqrt{n}|\beta_{n}| \sum_{k=0}^n\frac{\gamma_k}{|\beta_k|}\E(|\delta_k|\indic_{|X_k-x^*|>\epsilon})\\
&+\sqrt{n}|\beta_{n}| \sum_{k=0}^n\frac{\gamma_k}{|\beta_k|}\E(|\delta_k|\indic_{|X_k-x^*|\leq\epsilon})\\
&= T_1+T_2
\end{align}
avec des notations évidentes.
\subparagraph{Terme $T_1$}Ce premier terme est facile à contrôler. En effet comme $X_n\cvps x^*$
\begin{equation}
  \exists N,\ \forall n\geq N,\ \indic_{|X_n-x^*|>\epsilon}=0\text{ p.s.}\\
\end{equation}
Donc, presque sûrement, 
\begin{align}
   \sqrt{n}|\beta_{n}| \sum_{k=0}^n\frac{\gamma_k}{|\beta_k|}\E(|\delta_k|\indic_{|X_k-x^*|>\epsilon}) &=  \sqrt{n}|\beta_{n}| \sum_{k=0}^{N-1}\frac{\gamma_k}{|\beta_k|}\E(|\delta_k|\indic_{|X_k-x^*|>\epsilon})\\
  &= \bigo\left(\sqrt{n}\beta_{n}\right)\cvps 0.
\end{align}
\subparagraph{Terme $T_2$}Comme $\phi$ est $\mathcal{C}^1$ et $X_k\cvps[k]x^*$, $\delta_k=o(X_k-x^*)$, i.e. pour tout $h>0$ on peut choisir $\epsilon$ de sorte que pour $k$ assez grand $|\delta_k|\leq h|X_k-x^*|$ quand $|X_k-x^*|\leq\epsilon$. D'où
\begin{align}
  \sqrt{n}|\beta_{n}| \sum_{k=0}^n\frac{\gamma_k}{|\beta_k|}\E(|\delta_k|\indic_{|X_k-x^*|\leq\epsilon}) &\leq  \sqrt{n}|\beta_{n}| \sum_{k=0}^n\frac{\gamma_k}{|\beta_k|}\E(h|X_k-x^*|)\\
  &\leq  h\sqrt{n}|\beta_{n}| \sum_{k=0}^n\frac{\gamma_k}{|\beta_k|}\E(|X_k-x^*|)
  \intertext{On applique l'inégalité de Jensen, ce qui donne}
    &\leq  h\sqrt{n}|\beta_{n}| \sum_{k=0}^n\frac{\gamma_k}{|\beta_k|}\sqrt{\E(V_k)}\label{cv_reste_terme_2}
\end{align}
avec les notations de la preuve du théorème \ref{th_cvps}. Estimons $\E(V_n)$. On part de \eqref{decomp_carre_final}, qu'on rappelle :
\begin{equation*}
    \E_nV_{n+1} \leq V_n(1 + C''\gamma_n^2) + C''\gamma_n^2 + 2\gamma_n(X_n-x^*)(\phi(X_n)-\alpha).
\end{equation*}
On peut maîtriser le dernier terme de cette somme pour obtenir une récurrence plus simple. En effet :
\begin{align}
  \forall h>0,\ \exists n_0,\ \forall n\geq n_0,\ |\delta_n|&\leq h|X_n-x^*|\\
  \intertext{implique}
  \forall n\geq n_0,\  |\delta_n(X_n-x^*)|&\leq hV_n.
\end{align}
D'où, pour $n\geq n_0$, on a 
\begin{align}
  (X_n-x^*)(\phi(X_n)-\alpha) &=(X_n-x^*)(\phi'(x^*)(X_n-x^*)+\delta_n)\\
  &=-qV_n+\delta_n(X_n-x^*)\\
  &\leq V_n(h-q),
\end{align}
et donc \eqref{decomp_carre_final} devient
\begin{align}
  \E_nV_{n+1} &\leq V_n(1 + C''\gamma_n^2+2\gamma_n(h-q)) + C''\gamma_n^2.
  \intertext{En notant $v_n$ l'espérance de $V_n$ on en déduit que : }
  v_{n+1} &\leq v_n(1 + C''\gamma_n^2+2\gamma_n(h-q)) + C''\gamma_n^2.
  \intertext{Par récurrence sur $n$ on obtient}
  v_{n+1} &\leq w_n\left(v_{n_0}+\sum_{k=n_0}^n\frac{C''\gamma_k^2}{w_k}\right)
\end{align}
où $w_n = \prod_{k=n_0}^n(1 + C''\gamma_k^2+2\gamma_k(h-q))$. Et comme à l'étape 2 on peut montrer que
\begin{equation}
  w_nn^{2(q-h)}\xrightarrow[n\rightarrow\infty]{}  c>0.
\end{equation}
Si $0<h<q-1/2$, comme
\begin{align}
  \frac{1}{k^2w_k} &= \bigo(k^{2(q-h)-2}),
  \intertext{on a }
  \sum_{k=n_0}^n\frac{C''\gamma_k^2}{w_k} &= \bigo\left(n^{2(q-h)-1}\right).
  \intertext{On conclut ainsi que}
  v_n &= \bigo(1/n).
\end{align}
Toutes ces estimations permettent de revenir à \eqref{cv_reste_terme_2} et obtenir ainsi :
\begin{equation}
  \frac{\gamma_k}{|\beta_k|}\sqrt{v_k}=\bigo\left(k^{q\gamma-3/2}\right).
\end{equation}
Pour conclure il reste à discuter selon la valeur de l'exposant. Trois cas doivent être considérés : 
\begin{enumerate}
\item[Cas 1] $q\gamma-3/2<-1$ %% CV
\item[Cas 2] $q\gamma-3/2> -1$ %% DV
\item[Cas 3] $q\gamma-3/2 = -1$
\end{enumerate}
\subparagraph{Cas 1}\eqref{cv_reste_terme_2} donne un $\bigo(\sqrt{n}\beta_n)$, ce qui suffit pour avoir le résultat.
\subparagraph{Cas 2}Dans ce cas \eqref{cv_reste_terme_2} est de l'ordre de $\bigo(h\sqrt{n}\beta_nn^{q\gamma-1/2})$ pour $0<h<q-1/2$. C'est-à-dire un $h\bigo(1)$, ce qui nous permet aussi de conclure.
\subparagraph{Cas 3}L'estimation est en $\bigo(\log(n)n^{q\gamma-1/2})$ qui tend aussi vers zéro.
\paragraph{}
Dans tous les cas
\begin{equation}
  \sqrt{n}|\beta_{n}| \sum_{k=0}^n\frac{\gamma_k}{|\beta_k|}\E(|\delta_k|\indic_{|X_k-x^*|\leq\epsilon})\cvps 0,
\end{equation}
donc, finalement,
\begin{equation}
  \sqrt{n}\beta_{n}R_{n}\cvmoy 0,
\end{equation}
ce qui conclut cette dernière étape.
\paragraph{Conclusion}Deux termes dans \eqref{decomp} tendent vers zéro en loi et le troisième tend vers $\mathcal{N}(0,\sigma^2/(2q-1))$.
\end{proof}
