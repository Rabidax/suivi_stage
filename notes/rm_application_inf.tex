Nous allons expliquer comment le problème de l'estimation des paramètres d'un processus ponctuel peut se résoudre grâce à l'algorithme de Robbins-Monro.

Notre processus est de densité (par rapport au processus de Poisson standard)
\begin{align}
  f(x|\theta) &= \frac{\exp(\left<\theta, t(x)\right>)}{c(\theta)}\\
  \intertext{où $\theta$ est un vecteur de paramètres. Alors la log-vraisemblance a pour expression}
  l(\theta) &= \left<\theta, t(x)\right>-\log(c(\theta)).\\
  \intertext{Si on a plusieurs échantillons, il faut remplacer $t(x)$ par $\sum_i^nt(x_i)$ dans l'expression précédente. Si $c(\theta)$ est supposée différentiable, maximiser la vraisemblance revient à trouver $\theta^\star$ tel que}
  \nabla l(\theta^\star) &= 0.\\
  \intertext{Or, on a aussi que}
  \nabla l &= t(x)-\frac{\nabla c}{c}\\
  \intertext{où $x$ sont les données observées. On note $\phi$ la fonction $\theta\mapsto\nabla c(\theta)/c(\theta)$. Par définition, en notant $W$ la fenêtre d'observation on a que}
  c(\theta) &= \int_{x\subset W}\exp(\theta\cdot t(x))d\mu(x)
  \intertext{où $\mu$ est la loi du processus de Poisson standard. Si on peut dériver l'intégrale on a, en notant $\partial_i$ la $i$-ème dérivée partielle par rapport à $\theta$, on obtient}
  \partial_ic &= \int\partial_i \exp(\left<\theta, t(x)\right>)d\mu(x)\\
  &= \int t(x)_i\exp(\left<\theta, t(x)\right>)d\mu(x).\\
  \intertext{Et donc on a}
  \frac{\partial_ic}{c} &= \int t(x)_i\frac{\exp(\left<\theta, t(x)\right>)}{c(\theta)}d\mu(x).\\
  \intertext{Ce qui implique que}
  \frac{\nabla c}{c} &=\E_\theta(t(X))\label{rmForParamInference}
  \intertext{où $X$ suit $f(\cdot|\theta)$. On montre de la même manière que la jacobienne de $\phi$ est la matrice de covariance de $t(X)$. On cherche alors à résoudre}
  \phi(\theta) &= t(x)
\end{align}
où on ne connait $\phi$ qu'à travers \eqref{rmForParamInference}. On est donc bien dans le cas d'une recherche de niveau.

On peut alors chercher à approcher le maximum de vraisemblance $\hat{\theta}_{ML}$ par des itérations de
\begin{equation}
  \label{rm_application}
  \theta_{n+1} = \theta_{n}+\frac{\gamma}{n}(t(X_n)-t(x))
\end{equation}
avec $X_n\sim f(\cdot|\theta_n)$.
\subsection{Illustration numérique}
On simule $N=1000$ échantillons d'une gaussienne $\mathcal{N}(\mu,\sigma^2)$ où $\mu=1$ et $\sigma=2$. On choisit une valeur initiale pour $\theta$ : ici on a pris $\theta_0=(6,5.5)$. On effectue ensuite $10^5$ itérations de l'algorithme de Robbins-Monro, en prenant soin que la variance ne devienne pas négative.

D'abord on choisit le pas de la forme $\gamma/n$ avec $\gamma=10^{-3}$. Les résultats sont disponibles en figure~\ref{fig_rm1}. On voit dans la deuxième ligne de la figure que la statistique exhaustive $t(X)$ de la variable auxiliaire atteint un comportement qu'on pourrait qualifier d'équilibre : elle oscille autours des valeurs $t(x)$ des données. On peut remarquer, par exemple dans la trajectoire de la variance, que l'algorithme ne converge pas exactement vers les vraies valeurs du paramètres --- la variance vaut 3.875 après $10^5$ itérations, la vraie valeur étant 4. Ce qui est normal, l'algorithme de Robbins-Monro fournissant une procédure pour obtenir le maximum de vraisemblance d'un échantillon.
\begin{figure}
  \centering
  \includegraphics[scale=0.8]{../code/rmNormal_0001}
  \caption{Evolution de la valeur des paramètres et des composantes de $t(X)-t(x)$. Le facteur $\gamma$ du pas vaut $10^{-3}$.}
  \label{fig_rm1}
\end{figure}

La figure~\ref{fig_rm2} est générée exactement de la même manière que précédemment, sauf que le facteur du pas $\gamma$ est choisi égal à $1$. La convergence est ici bien plus lente. Au vu des résultats, la remarque faite plus haut est à tempérer. A la section \ref{section_choixpas}, nous justifiions la restriction de notre preuve au cas de la normalité asymptotique grâce à la possibilité de prendre $\gamma$ suffisamment grand pour s'assurer d'être dans ce cas. Mais comme on le voit maintenant, cela peut conduire à une convergence lente.
\begin{figure}
  \centering
  \includegraphics[scale=0.8]{../code/rmNormal_1}
  \caption{Evolution de la valeur des paramètres et des composantes de $t(X)-t(x)$. Le facteur $\gamma$ du pas vaut $1$.}
  \label{fig_rm2}
\end{figure}

On pourrait aussi étudier la dépendance à la condition initiale, ou s'assurer de la convergence asymptotique.
