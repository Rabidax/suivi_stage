Dans ce chapitre, nous introduisons les notions nécessaires à la manipulation de modèles spatiaux. Nous présentons d'abord le cadre général, puis le modèle de Poisson (équivalent spatial d'une loi uniforme) ainsi que la mesure de référence associée, et enfin quelques modèles plus sophistiqués.
\section{Cadre général}
Soit $(\Omega, \mathcal{F}, \P)$ un espace de probabilité, et $W$ un compact de $\R^d$ qu'on munit de sa tribu borélienne $\mathcal{B}$. On note $M_p$ l'ensemble des mesures de comptages sur $W$, i.e. les mesures de Radon à valeurs dans $\N$. On munit cet ensemble de la tribu $\mathcal{M}_p$ engendrée par les
\begin{equation*}
  \left(\pi_B:\left\{
  \begin{aligned}
    M_p&\longrightarrow \N\\
    \mu &\longmapsto \mu(B)
  \end{aligned}\right.\right)_{B\in\mathcal{B}  }.
\end{equation*}
\begin{defn}
  Un processus ponctuel est une variable aléatoire à valeur dans $(M_p,\mathcal{M}_p  )$.
\end{defn}
Dans ce qui suit, nous dirons "processus" au lieu de "processus ponctuel". Pour un borélien $B$ on note $N(B)$ la variable aléatoire $\pi_B\circ N$. C'est le nombre (aléatoire) de points présents dans $B$.
\begin{defn}
  La mesure d'intensité d'un processus aléatoire $N$ est la mesure $\nu$ définie par
  \begin{equation*}
    \forall B\in\mathcal{B},\ \nu(B) = \E(N(B)).
  \end{equation*}
\end{defn}
\begin{defn}
  Un processus est dit fini quand $N(W)<\infty$ p.s.
\end{defn}
\paragraph{Remarque}Cette construction du concept de processus ponctuel passe par des mesures aléatoires mais dans notre cas, on peut aussi le comprendre comme des ensembles aléatoires de points de $W$.
\section{Processus de Poisson}
\label{section_poisson}
Un exemple important de processus fini est le suivant. On note $\mathcal{P}_\lambda$ la distribution de Poisson classique de paramètre $\lambda$. Précédé d'une variable aléatoire, le symbole $\sim$ désigne une égalité de loi.
\begin{defn}
  Soit $\nu$ une mesure finie. Un processus de Poisson est un processus $N$ vérifiant
  \begin{enumerate}
  \item $\forall B\in\mathcal{B},\ N(B)\sim\mathcal{P}_{\nu(B)}$
    \item Pour tout $k\in\N$ et pour tout boréliens $B_1,\ldots,B_k$, les $N(B_1),\ldots,N(B_k)$ sont indépendants.
  \end{enumerate}
  $\nu$ est alors l'intensité du processus.
\end{defn}
\begin{defn}
  Quand $\nu$ est proportionelle à la mesure de Lebesgue (notée $\lambda$ dans la suite) sur $W$, on parle de processus de Poisson \emph{homogène}. On parle de processus de Poisson \emph{standard} lorsque cette proportionalité est une égalité.
\end{defn}
\begin{prop}
  Soient $N_1$, $N_2$ deux processus de Poisson de même intensité. Alors $N_1$ et $N_2$ ont même loi.
\end{prop}
La proposition qui suit permet de simuler facilement la réalisation d'un processus de Poisson d'intensité donnée.
\begin{prop}[Construction]
  \label{poiss_constr}
  On se donne
  \begin{itemize}
  \item Une mesure finie $\nu$
  \item $\tau$ de loi $\mathcal{P}_{\nu(W)}$
  \item $X_1,X_2,...$ à valeurs dans $W$ de même loi $\frac{\nu}{\nu(W)}$
  \end{itemize}
  tels que $\tau,X_1,X_2,\ldots$ soient indépendants. Alors le processus
  $\sum_{i=1}^\tau\delta_{X_i}$ est un processus de Poisson et son intensité est $\nu$.
\end{prop}
\paragraph{Exemple}
Pour simuler un processus de Poisson homogène (d'intensité constante $\nu>0$) sur le carré $[0,1]^2$, il suffit d'obtenir un nombre de points $n$ en simulant $\mathcal{P}_\nu$, puis de choisir uniformément et de manière indépendante $n$ points dans $[0,1]^2$. Voir la figure~\ref{fig_poiss_homog} pour un exemple. On a utilisé le paquet \verb|R| nommé \verb|spatstat| \cite{baddeley2015spatial} qui est une véritable boîte à outils pour les statistiques spatiales.\\
A titre de comparaison, la figure~\ref{fig_poiss_inhomo} contient des réalisations d'un processus de Poisson inhomogène.

\begin{figure}
  \centering
  \includegraphics[scale=0.3]{../code/learningSpatstat/poisspp_50.jpg}
  \caption{2 réalisations d'un processus de Poisson homogène d'intensité 50.}
  \label{fig_poiss_homog}
\end{figure}
\begin{figure}
  \centering
  \includegraphics[scale=0.3]{../code/learningSpatstat/poisspp_squared.jpg}
  \caption{2 réalisations d'un processus de Poisson d'intensité $(x,y)\mapsto 100(x^2+y^2)$}
  \label{fig_poiss_inhomo}
\end{figure}

%% Loi du processus de Poisson
\begin{prop}
  La loi d'un processus de Poisson standard $X$ est 
  \begin{equation}
    \label{loi_poiss}
     \P(X\in F) = e^{-\lambda(W)}\sum_{n\geq 0}\frac{1}{n!}\int_{W^n}\indic\left[\sum_{i=1}^n\delta_{x_i}\in F\right]dx_1\ldots dx_n
  \end{equation}
  pour tout ensemble mesurable $F$ de $\mathcal{M}_p$.
\end{prop}
\begin{proof}[Idée de démonstration]
  Conditionner selon la valeur de $N(W)$ et utiliser la proposition \ref{poiss_constr}.
\end{proof}
On note alors $\mu$ cette mesure sur $M_p$, ce qui permet d'allèger l'écriture d'intégrales. Par exemple \eqref{loi_poiss} devient
\begin{equation}
  \P(X\in F) = \int\indic_F(x)d\mu(x),
\end{equation}
où $x$ est un élément de $M_p$.
%% Densité vis-à-vis du pPoiss
\begin{defn}
  On dit qu'un processus ponctuel $X$ admet une densité par rapport au processus de Poisson standard s'il existe $f:M_p\longrightarrow \R_+$, intégrable pour $\mu$, telle que la loi de $X$ soit
  \begin{align}
    \P(X\in F) &= e^{-\lambda(W)}\sum_{n\geq 0}\frac{1}{n!}\int_{W^n}\indic\left[\sum_{i=1}^n\delta_{x_i}\in F\right]f\left(\sum_{i=1}^n\delta_{x_i}\right)dx_1\ldots dx_n,\\
    \intertext{i.e.}
    \P(X\in F) &= \int_F f(x)d\mu(x).
  \end{align}
\end{defn}

\paragraph{Remarque}
En pratique, on ne connaît $f$ qu'à une constante de normalisation près.   Cette constante étant impossible à déterminer analytiquement, on est obligé de développer des techniques spécifiques à ces modèles non normalisables. On notera $\propto$ cette relation de proportionalité.
\paragraph{Exemple}
Un processus de Poisson homogène a pour densité $f(x)\propto \beta^{n(x)}$, en notant
\begin{equation}
  \label{def_nb_pts}
  n(x) = x(W)
\end{equation}
(on rappelle que $x$ est une mesure) et $\beta=\frac{\nu(W)}{\lambda(W)}$ est le paramètre.

\section{Modèles d'interaction}
Nous présentons dans cette section quelques processus ponctuels particuliers. Dans la suite, une densité sera toujours implicitement par rapport au processus de Poisson standard. Ces modèles sont aussi tous dans la familles exponentielles, i.e. ont des densités de la forme
\begin{equation}
  f(x)\propto e^{\theta\cdot t(x)}
\end{equation}
où $\theta$ est un vecteur de paramètres et $t(x)$ une statistique exhaustive. Cette forme sera dite canonique.
\subsection{Modèle de Strauss}
\begin{defn}
  Pour un réel $r>0$ on appelle modèle de Strauss le processus ponctuel de densité
  \begin{equation}
    f(x)\propto \beta^{n(x)}\gamma^{s_r(x)},
  \end{equation}
  où
  \begin{itemize}
  \item $\beta >0$ et $\gamma\in[0,1]$ sont les paramètres du modèle,
    \item $n(x)$ est défini en \eqref{def_nb_pts} et \[s_r(x)\coloneqq \sum_{\{a,b\}\subset x}\indic[\left\|a-b\right\|\leq r].\]
  \end{itemize}
\end{defn}
\paragraph{Remarque}C'est l'intégrabilité de la densité qui impose la contrainte sur $\gamma$.
\paragraph{Interprétation}$s_r(x)$ est le nombre de paires de points distants d'au plus $r$. La densité favorise les cas où $s_r(x)$ est petit, i.e. les points ont tendance à se repousser. Les deux cas extrêmes $\gamma=1$ et $\gamma=0$ correspondent respectivement au processus de Poisson (homogène de paramètre $\beta$) et à un processus de Poisson où on impose une distance d'au moins $r$ entre deux points.
\paragraph{Exemple}Le paquet \verb|spatstat| dévelopé par les auteurs de \cite{baddeley2015spatial} contient un jeu de donnée auquel se modèle s'applique bien : la position d'arbres d'une forêt suédoise. Nous ne rentrons pas dans les détails de la discussion, mais on peut déjà imaginer que ces arbres se disputent des ressources (place pour leur racines, lumière) et donc ont tendance à pousser à distance les uns des autres.
\subsection{Modèle d'interaction par aire}
\begin{defn}
  Pour un réel $r>0$ on appelle modèle d'interaction par aire le processus ponctuel de densité
  \begin{equation}
    f(x)\propto \beta^{n(x)}\gamma^{-A_r(x)},
  \end{equation}
  où
  \begin{itemize}
    \item $\beta >0$ et $\gamma>0$ sont les paramètres du modèle,
    \item $n(x)$ est défini comme plus haut et $A_r(x)\coloneqq \left|W\cap\cup B(x_i,r)\right|$.
  \end{itemize}
\end{defn}
\paragraph{Interprétation}Selon la position de $\gamma$ par rapport à 1, le comportement du processus change :
\begin{itemize}
\item le cas $\gamma=1$ donne un processus de Poisson homogène,
\item le cas $\gamma>1$ favorise $A_r(x)$ grand, i.e. un comportement répulsif,
  \item et quand au cas $\gamma<1$, il favorise des configurations agrégées, ou des alignements.
\end{itemize}
