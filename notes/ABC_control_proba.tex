Notre objectif est d'obtenir un résultat de convergence en loi pour l'algorithme ABC Shadow. Pour le dire autrement, on veut montrer qu'asymptotiquement, on échantillone bien la loi a posteriori des paramètres. Notre approche est la suivante :
\begin{enumerate}
\item Montrer que l'écart (en variation totale) entre la chaîne idéale et la chaîne shadow est bien contrôlé en fonction du pas, et ce peu importe la valeur de la variable auxiliaire.
  \item Sous une condition liant le pas $\Delta$ et la taille $n$ des blocs d'itérations, utiliser le résultat précédent et l'ergodicité de la chaîne idéale pour montrer la convergence en loi.
\end{enumerate}
\section{Contrôle de l'écart des noyaux}

La preuve de \cite[Proposition 1]{StoicaEtAl2017} montre qu'on a, pour $n$ et $\Delta$ fixés et sous certaines hypothèses sur $x$ qui est généré selon la loi $p$ du modèle
\begin{equation}
  \label{prop1}
  \sup_{\theta\in\Theta}\left\|P_i^n(\theta,\cdot)-P_s^n(\theta,\cdot)\right\|\leq C_3(x,p,\Theta)n\Delta
\end{equation}
où $C_3(x,p,\Theta)$ est définie par
\begin{equation}
  \label{def_C3}
  C_3(x,p,\Theta) = \frac{M(x)}{m(x)}\left(3+2\frac{M(x)}{m(x)}\right)\sup_{\theta\in\Theta}\left\|\nabla_\theta p(x|\theta)\right\|.
\end{equation}
Dans cette expression, on a noté $m(x)$ le minimum de vraisemblance $\inf_{\theta\in\Theta}p(x|\theta)$, $M(x)$ le maximum de vraisemblance et $\nabla_\theta$ le gradient par rapport à $\theta$.
Dans le cas des modèles exponentiels, avec une statistique exhaustive $t$, le gradient de $p(x|\cdot)$ a pour expression
\begin{equation}
  \nabla_\Theta p(x|\theta) = p(x|\theta)\left(t(x)-\E_\theta [t(X)]\right).
\end{equation}
Donc \eqref{def_C3} peut se réécrire
\begin{equation}
  C_3(x,p,\Theta) = \frac{M(x)}{m(x)}\left(3+2\frac{M(x)}{m(x)}\right)\sup_{\theta\in\Theta}\left\|t(x)-\E_\theta [t(X)]\right\|.
\end{equation}

Le problème de \eqref{prop1}, c'est que $C_3$ dépend de $x$ et donc quand on réactualise la variable auxiliaire, la majoration évolue. On voudrait majorer $C_3$ indépendamment de la variable auxiliaire.
\subsection{Hypothèses sur le modèle}
On note $p(x|\theta)$ la densité normalisée du modèle, $f$ la densité non-normalisée et $c$ la constante de normalisation. On suppose que
\begin{enumerate}[(H1)]
  \item $\theta\mapsto \E_\theta\left\|t(x)\right\|$ est définie et bornée sur $\Theta$,\label{esp_norme1}
  \item $\theta\mapsto \E_\theta t(x)$ est définie et bornée sur $\Theta$.\label{esp_norme2}
\end{enumerate}
\subsubsection{Réalisme des hypothèses}
Les paragraphes qui suivent étudient le réalisme des hypothèses sur des cas concrets.
\paragraph{Gaussienne}Soit $\mu\in\R$, $\sigma>0$ et $X\sim\mathcal{N}(\mu,\sigma^2)$. Alors $t(X)=(X,X^2)$, $\theta=(\mu/\sigma^2,-1/\sigma^2)$ et dans ce cas $\E_\theta t(x)=(\mu,\sigma^2+\mu^2)$, ce qui est une fonction continue de $\theta$, donc bornée si $\Theta$ est compact. Par l'inégalité de Jensen, la fonction mentionée en \ref{esp_norme1} est majorée par $\left(\E_\theta x^2\right)^{1/2}=\sqrt{\sigma^2+\mu^2}$.\\
Ces hypothèses sont valables pour une gaussienne.

Pour les modèles de processus ponctuels on fait des hypothèses qui impliquent celle-ci dessus pour (au moins) les deux modèles présentés au premier chapitre :
\begin{enumerate}[(H1), resume]
\item $\forall \theta,\ \exists M_\theta >0,\ \forall x,\ f(x|\theta)\leq M_\theta^{n(x)}$,\label{dom_poiss}
\item $\theta\mapsto M_\theta$ est continue.\label{dom_poiss_cont}
\end{enumerate}
\paragraph{Modèle de Strauss}Par définition $f(x|\theta)=\beta^{n(x)}\gamma^{s_r(x)}$ pour $\beta>0$ et $\gamma\in[0,1]$. Comme $\gamma^{s_r(x)}\leq 1$, $M=(\beta,\gamma)\mapsto \beta$ convient.
\paragraph{Modèle d'interaction par aires}
Ici, $f(x|\theta)=\beta^{n(x)}\eta^{-C_r(x)}$ avec $\beta>0$, $\eta>0$. Comme $0\leq -C_r(x)\leq n(x)$
\begin{equation*}
  M=(\beta,\eta)\mapsto
  \begin{cases}
    \beta\eta&\text{si}\ \eta\geq 1\\
    \beta&\text{sinon}
  \end{cases}
\end{equation*}convient.\\
Ces hypothèses impliquent \ref{esp_norme1}-\ref{esp_norme2}. En effet
\begin{align}
  0&\leq s_r(x) \leq {n(x) \choose 2}\leq \frac{n(x)^2}{2},\label{strauss_domination_explicite}
  \intertext{et comme vu plus haut}
  0&\leq -C_r(x)\leq n(x).\label{aire_domination_explicite}
  \intertext{Or par convergence dominée, grâce à \ref{dom_poiss}\ref{dom_poiss_cont}}
  \E[n(x)^p]&=\int n(x)^pp(x|\psi)\mu(dx)\label{moments_nb_pts}
\end{align}
est une fonctionc continue de $\psi$ quelque soit $p\in\N^*$. C'est-à-dire que tout moment de $n$ est une fonction continue des paramètres. Donc en utilisant \eqref{strauss_domination_explicite} et \eqref{aire_domination_explicite} on voit que chaque composante de $\E t(x)$ est bornée et donc \ref{esp_norme2} est vérifiée. Ces deux équations impliquent aussi que toute composante $t_i$ de $t$ est positive et majorée par un polynôme en $n(x)$, que l'on note $P_i$. En notant $P(n(x))$ le vecteur des $P_i(n(x))$ on a
\begin{align}
  \E\left\|t(x)\right\| &\leq   \E\left\|P(n(x))\right\|\\
  &\leq \left(\sum_i\E\left[P_i(n(x))^2\right]\right)^{1/2}
\end{align}
Le terme sous la racine est une combinaison linéaire de moments de $n(x)$ ce qui permet de conclure : \ref{esp_norme1} est vérifiée.
\subsection{Résultat}
\begin{lemme}
  \label{lemme_controle_proba}
  Sous les hypothèses \ref{esp_norme1}-\ref{esp_norme2}, on a
  \begin{equation}
    \forall \varepsilon,\exists M>0, \forall \psi\in\Theta, x\sim p(\cdot|\psi)\Rightarrow \P(\left\|t(x)\right\|>M)<\varepsilon 
  \end{equation}
\end{lemme}
\begin{proof}
  Soit $\varepsilon,M>0$. Par l'inégalité de Markov :
  \begin{align}
    \P(\left\|t(x)\right\|>M)&\leq\frac{\E_\psi(\left\|t(x)\right\|)}{M}\\
    &\leq \frac{\sup_{\psi\in\Theta}\E_\psi(\left\|t(x)\right\|)}{M}.
  \end{align}
  Il suffit alors de choisir $M>\left(\sup_{\psi\in\Theta}\E_\psi(\left\|t(x)\right\|\right)/\varepsilon$.
\end{proof}
Ce lemme nous assure qu'en probabilité $t(x)$ ne peut être trop grand, et ce de manière uniforme par rapport au paramètre générant la variable auxiliaire.
\begin{prop}
  \label{prop_controle_C3_proba}
  On fait les hypothèses du lemme \ref{lemme_controle_proba}. Pour tout $\varepsilon>0$, il existe $C>0$ telle que
  \begin{equation}
    \forall \psi\in\Theta, x\sim p(\cdot|\psi)\Rightarrow \P(C_3(x)>C)<\varepsilon.
  \end{equation}
\end{prop}
\begin{proof}
  Soit $\varepsilon>0$. Pour simplifier, on note $R(x)$ le ratio $\frac{M(x)}{m(x)}$ et $S(x)$ la quantité $\sup_{\theta\in\Theta}\left\|t(x)-\E_\theta [t(X)]\right\|$. Sur l'événement $\left\{\left\|t(x)\right\|\leq M\right\}$, on a
  \begin{align}
    S(x) &\leq \left\|t(x)\right\|+\sup_\theta\left\|\E_\theta t(X)\right\|\\
    &\leq M + \sup_\theta\left\|\E_\theta t(X)\right\|.
  \end{align}

  Quant à $R(x)$, on a
  \begin{align}
    R(x) &= \frac{\sup_\theta p(x|\theta)}{\inf_\theta p(x|\theta)}\\
    &= \frac{\sup c}{\inf c}e^{t(x)\cdot(\theta_s-\theta_i)}
    \intertext{où $\theta_s,\theta_i$ viennent de la continuité de $p(x|\cdot)$. D'où par l'inégalité de Cauchy-Schwarz, en notant $D$ le diamètre de $\Theta$ on voit que }
    R(x) &\leq \frac{\sup c}{\inf c}e^{D\left\|t(x)\right\|}\\
    &\leq \frac{\sup c}{\inf c}e^{DM}.
  \end{align}
  D'où l'existence de $C>0$ indépendante de $x$ telle que
  \begin{equation}
    \P(C_3(x)\leq C)\geq P(\left\|t(x)\right\|\leq M)\geq 1-\varepsilon.
  \end{equation}
\end{proof}
\section{Convergence d'ABC Shadow}
Avant d'énoncer un résultat nous devons mettre en place quelques notations. Pour tout entier $k$, toute suite $(n_j)_{1\leq j\leq k}$ d'entiers, toute suite $(\Delta_j)_{1\leq j\leq k}$ de réels strictement positifs, et toute suite $(x_j)_{1\leq j\leq k}$ de réalisations de la variable auxiliaire selon des paramètres quelconques, on note
\begin{equation*}
  \mathcal{P}_{i,k}\coloneqq P_{i,\Delta_1}^{n_1}\cdots P_{i,\Delta_k}^{n_k}
\end{equation*}
le produit de ces $k$ noyaux de la chaîne idéale. De même pour la chaîne shadow en remplaçant "i" par "s".\\
On sait que quelque soit la valeur de la variable auxiliaire ou de $\Delta$, la chaîne idéale est uniformément ergodique avec la même distribution stationaire, que l'on nomme $\pi$. Donc
\begin{equation}
  \exists \rho\in ]0,1[,\forall n\ \left\|P_{i,\Delta}^n-\pi\right\|\leq2\rho^n.
\end{equation}
Ici la norme est la norme d'opérateur induite par la norme de variation totale. Pour simplifier, on note de la même manière une mesure $\mu$ et le noyau $(x,A)\mapsto \mu(A)$. Pour un paramètre indicé $\Delta_k$ on notera $\rho_k$ pour marquer la dépendance de $\rho$ aux paramètres du noyau. Ce taux de convergence $\rho$ est aussi appelé coefficient d'ergodicité ou coefficient de Dobrushin (cf. \cite{mitrophanov_2005} pour des rappels de la définition et des propriétés associées).

\begin{prop}
  \label{prop_cv_proba}
  Soit $\varepsilon,\delta>0$
  \begin{equation}
    \exists C>0, \forall k\in\N,\ \frac{\ln(\varepsilon/4)}{\ln(\rho_k)}\leq n_k\leq\frac{\varepsilon/2}{C\Delta_k}
    \Rightarrow\P\big(\left\|\mathcal{P}_{s,k}-\pi\right\|\leq \varepsilon\big)\geq 1-\delta.
  \end{equation}
\end{prop}
\begin{proof}
  Soit $\varepsilon,\delta>0$. On se donne une suite $(P_{s,\Delta_j}^{n_j})_{1\leq j\leq k}$ une suite de noyau de la chaîne shadow, ainsi que les noyaux de la chaîne idéale correspondants. On a 
\begin{align}
    \left\|\mathcal{P}_{s,k}-\pi\right\|&\leq\left\|\mathcal{P}_{s,k}-\mathcal{P}_{s,k-1}P_{i,\Delta_k}^{n_k}\right\|+\left\|\mathcal{P}_{s,k-1}P_{i,\Delta_k}^{n_k}-\pi\right\|\\
    &\leq \underbrace{\left\|\mathcal{P}_{s,k-1}\right\|}_{=1}\left\|P_{s,\Delta_k}^{n_k}-P_{i,\Delta_k}^{n_k}\right\|+\left\|\mathcal{P}_{s,k-1}P_{i,\Delta_k}^{n_k}-\pi\right\|.
    \intertext{Par ergodicité uniforme de $P_{i,\Delta_k}$ le deuxième terme est majoré ainsi :}
    \left\|\mathcal{P}_{s,k}-\pi\right\|&\leq \left\|P_{s,\Delta_k}^{n_k}-P_{i,\Delta_k}^{n_k}\right\|+2\rho_k^{n_k}.\label{abc_control_1}
\end{align}
A $\delta$ fixé, la proposition \ref{prop_controle_C3_proba} donne une constante $C$ telle que sur l'évènement $\{C_3\leq C\}$, 
\begin{equation}
  \left\|P_{s,\Delta_k}^{n_k}-P_{i,\Delta_k}^{n_k}\right\|\leq Cn_k\Delta_k.\label{abc_control_2}
\end{equation}
Donc si $n_k$ vérifie la double inégalité de l'énoncé alors, en combinant \eqref{abc_control_1} et \eqref{abc_control_2}, on a
\begin{equation}
  \left\|\mathcal{P}_{s,k}-\pi\right\|\leq \varepsilon
\end{equation}
sur un évènement de probabilité d'au moins $1-\delta$.
\end{proof}
Il reste bien entendu à montrer qu'une suite $(n_k,\Delta_k)_k$ vérifiant la condition existe bel et bien. Une piste d'exploration du problème est la suivante. Si on reformule les conditions séparemment on veut
\begin{equation}
  \begin{cases}
    n_k\Delta_k\leq\frac{\varepsilon}{2C}\\
    n_k\ln(\rho_k)\leq \ln(\varepsilon/4).
  \end{cases}
\end{equation}
Si on choisit $\Delta_k = \frac{\varepsilon}{2Cn_k}$, on se ramène à la condition
\begin{equation}
  \frac{\ln(\rho_k)}{\Delta_k}\leq\frac{2C\ln(\varepsilon/4)}{\varepsilon}.
\end{equation}
Cette inégalité concerne l'évolution du taux de convergence de $P_{i,\Delta}$ quand $\Delta\rightarrow 0}$. Pour pouvoir appliquer concrétement la proposition \ref{prop_cv_proba}, on peut donc par exemple demander que le modèle donne lieu à une chaîne idéale telle que
    \begin{equation}
      \label{vitesse_dobrushin}
      \frac{\ln(\rho_\Delta)}{\Delta}\xrightarrow[\Delta\rightarrow 0]{}-\infty,
    \end{equation}
uniformément en la valeur de la variable auxiliaire.
\subsection{Perspectives}
Pour continuer sur le chemin tracé précédemment, il faudrait pouvoir évaluer pour les modèles qui nous intéressent si on peut obtenir \eqref{vitesse_dobrushin}. Estimer le taux de convergence vers la distribution stationnaire est encore l'objet de discussions. Comme intuitivement on imagine que $\rho_\Delta\xrightarrow[\Delta\rightarrow 0]{}1$, on veut s'assurer que cette convergence se fait lentement. On aurait alors un résultat justifiant (en probabilité) l'algorithme de l'article original \cite{StoicaEtAl2017}.

L'étape suivante serait d'obtenir un résultat de convergence de la quantité $\left\|\mathcal{P}_{s,k}-\pi\right\|$ en probabilité. Pour le moment en effet les paramètres $n,\Delta$ dépendent de $\varepsilon,\delta$.
