\documentclass{beamer}

%% Packages
\usetheme{Warsaw}
%% \usecolortheme{wolverine}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}

\input{../commandes_perso}
%% Titre
\title{Inférence pour les processus ponctuels}
\subtitle{ABC Shadow}
\author{Jean-Léon HENRY}
\institute{Université Lyon 1}

%% Frames plan auto
\AtBeginSection[]
{
  \begin{frame}
    \frametitle{Table des matières}
    \tableofcontents[currentsection]
  \end{frame}
}
%% Remove navigation buttons
\beamertemplatenavigationsymbolsempty

\begin{document}

\frame{\titlepage}
\begin{frame}
\frametitle{Table des matières}
\tableofcontents
\end{frame}

\section{Introduction}
\begin{frame}
  Exemples de données spatiales
  \begin{itemize}
  \item la localisation d'incidents de délinquance dans une ville,
  \item l'emplacement de failles géologiques,
  \item la distribution des arbres dans une forêt,
  \item les étoiles dans une galaxie,
  \item etc.
  \end{itemize}
  $\rightarrow$ processus ponctuels et modèles afférents.
\end{frame}

\subsection{Modèles}
\begin{frame}
  Fenêtre d'observation $W\subset\R^n$.
  \begin{block}{Processus de Poisson}
    On se donne
    \begin{itemize}
    \item $\tau\sim\mathcal{P}_{|W|}$
      \item $X_1,X_2\ldots$ i.i.d. de loi uniforme sur $W$.
    \end{itemize}
    avec $\tau,X_1,\ldots$ indépendants. Alors $\sum_{i=1}^\tau\delta_{X_i}$ est appelé processus de Poisson (standard).
  \end{block}
\end{frame}

\begin{frame}
  \begin{block}{Modèle de Strauss}
      Pour un réel $r>0$ on appelle modèle de Strauss le processus ponctuel de densité
  \begin{equation*}
    f(x)\propto \beta^{n(x)}\gamma^{s_r(x)},
  \end{equation*}
  \end{block}
  où
  \begin{itemize}
  \item $\beta >0$ et $\gamma\in[0,1]$ sont les paramètres du modèle,
    \item $n(x)$ est le nombre de points de la configuration et \[s_r(x)\coloneqq \sum_{\{a,b\}\subset x}\indic[\left\|a-b\right\|\leq r].\]
  \end{itemize}
  $\rightarrow$ comportements répulsifs.
\end{frame}

\begin{frame}
  \begin{block}{Modèle d'interaction par aire}
     Pour un réel $r>0$ on appelle modèle d'interaction par aire le processus ponctuel de densité
  \begin{equation*}
    f(x)\propto \beta^{n(x)}\gamma^{-A_r(x)},
  \end{equation*}
  \end{block}
  où
  \begin{itemize}
    \item $\beta >0$ et $\gamma>0$ sont les paramètres du modèle,
    \item $n(x)$ est défini comme plus haut et $A_r(x)\coloneqq \left|W\cap\cup B(x_i,r)\right|$.
  \end{itemize}
  $\rightarrow$ Le signe de $\gamma-1$ dicte le comportement.
    \begin{alertblock}{Attention}
    Constantes de normalisation inconnues.
  \end{alertblock}
\end{frame}

\section{Gradient stochastique}
\subsection{Principe}
\begin{frame}
  Soit $Y$ des observations. Dans un modèle exponentiel $f(x)\propto e^{\theta\cdot t(x)}$, l'équation de la vraisemblance est
  \begin{equation*}
    \E_\theta\left(t\right)=t(Y).
  \end{equation*}
  Le gradient stochastique propose comme méthode d'approximation du maximum de vraisemblance : 
  \begin{equation*}
    \left\{
  \begin{aligned}
    &\theta_{n+1} = \theta_n + \gamma_n\left(t(X_n)-t(Y)\right),\\
    &\theta_0\in L^2,
  \end{aligned}
  \right.
  \end{equation*}
  où $X_n\sim f(\cdot|\theta_n)$ et $\gamma_n$ est une suite de pas à choisir.
\end{frame}
\begin{frame}
  On peut généraliser la procédure. Soit $\phi:\R\rightarrow\R$ et $\alpha$ donné. On cherche $x^*$ tel que $\phi(x^*)=\alpha$. Alors on approxime $x^*$ par
  \begin{equation*}
    \left\{
  \begin{aligned}
    &X_{n+1} = X_n + \gamma_n\left(Y_{n+1}-\alpha\right),\\
    &X_0\in L^2.
  \end{aligned}
  \right.
  \end{equation*}
  où $(Y_{n})_{n\geq 1}$ est une suite de variable aléatoire à choisir.
\end{frame}
\subsection{Résultats}
\begin{frame}
  \frametitle{Convergence}
  \begin{block}{Hypothèses}
    \begin{enumerate}%%[(H1)]
  \item $\phi$ continue \label{hyp_ps_1}
  \item $\forall x\neq x^*,\ (x-x^*)(\phi(x)-\alpha)<0$\label{hyp_ps_2}
  \item $\forall n\geq0,\ \E_nY_{n+1}=\phi(X_n)$\label{hyp_ps_3}
  \item $\exists C>0,\ \forall n\geq0,\ \E_nY_{n+1}^2 \leq C(1+X_n^2)    $\label{hyp_ps_moment}
  \item $\gamma_n$ décroît vers zéro, $\sum_{n\geq 0}\gamma_n = \infty$, $\sum_{n\geq 0}\gamma_n^2<\infty$\label{hyp_ps_end}
\end{enumerate}
  \end{block}
  \begin{block}{Résultat}
    Sous ces hypothèses, $X_n\cvps x^*$
  \end{block}
\end{frame}
\begin{frame}
  \frametitle{Distribution de l'erreur}
  \begin{block}{Hypothèses supplémentaires}
    \begin{enumerate}
    \item $\phi\in\mathcal{C}^2$
    \item $\exists p>2,\ \sup_n\E_n\left(|Y_{n+1}-\phi(X_n)|^p\right)<\infty  $\label{hyp_tlc_lindenberg}
    \item $\sigma^2\coloneqq\lim_n\left(\E_nY_{n+1}^2-\phi(X_n)^2\right)$ existe\label{hyp_tlc_end}
      \item $\gamma_n=\gamma/n$
    \end{enumerate}
  \end{block}
  On note $q=-\phi'(x^*)\geq 0$.
  \begin{block}{Résultat}
    Avec tout ces hypothèses, et si $q\gamma>1/2$,
    \begin{equation*}
      \sqrt{n}\left(X_n-x^*\right)\cvlaw\mathcal{N}\left(0,\frac{\sigma^2\gamma^2}{2q\gamma-1}\right)
    \end{equation*}
  \end{block}
\end{frame}
\begin{frame}
  \frametitle{Remarques}
  \begin{itemize}
  \item Méthode historique : beaucoup de variantes sur ce principe, hypothèses plus faibles,...
  \item Comment choisir $(Y_n)$ et le pas $\gamma_n$ ? Dans le cas des processus ponctuels, nécessite de pouvoir simuler le modèle facilement.
  \item Approche le maximum de vraisemblance.
  \end{itemize}
\end{frame}
\section{ABC Shadow}
\begin{frame}
  \frametitle{Contexte bayésien}
  Soit $y$ les observations, $\Theta$ l'espace des paramètres.
  
  Objet d'intérêt : loi à posteriori des paramètres $f(\theta|y)$ qui est liée au modèle $f$ par
  \begin{equation*}
    f(\theta|y)\propto f(y|\theta)f(\theta),
  \end{equation*}
  où $f(\theta)$ est la loi à priori des paramètres.
  
  Pour simplifier :
  \begin{itemize}
  \item $\Theta$ est compact,
  \item $f(\theta)$ est uniforme.
  \end{itemize}
  Loi compliquée $\rightarrow$ simulation par Metropolis-Hastings.
\end{frame}

\subsection{Méthode}
\begin{frame}
  \frametitle{Principe}
  \footnotetext{Stoica, R.S., Philippe, A., Gregori, P. et al. Stat Comput (2017) 27: 1225.}
  
  On utilise deux chaînes de Markov.
  \begin{enumerate}
  \item une chaîne "idéale" : bonnes propriétés, mais non implémentable
    \item une chaîne "shadow" : implémentable, approche l'idéale.
  \end{enumerate}
  Chaînes à deux paramètres :
    \begin{enumerate}
    \item $x$ est une réalisation du modèle $f(\cdot|\psi)$
      \item $\Delta>0$
    \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Principe}
  \begin{block}{Chaîne idéale}
    Chaîne de Markov donnée par Metropolis-Hastings appliqué à:
    \begin{itemize}
    \item Loi instrumentale : $q_{x,\Delta}(\theta\rightarrow\psi)=\frac{f(x|\psi)\indic_{B(\theta,\Delta/2)}(\psi)}{I(\theta,\Delta,x)}$
      \item Loi cible : $\pi=f(\cdot|y)$.
    \end{itemize}
  \end{block}
  \begin{block}{Chaîne shadow}
    Chaîne de Markov donnée par Metropolis-Hastings modifié :
    \begin{itemize}
      \item $q_\Delta(\theta\rightarrow\cdot)=\frac{\indic_{B(\theta,\Delta/2)}}{|B(\theta,\Delta/2)|}$
      \item $\pi=f(\cdot|y)$
      \item $\alpha(\theta\rightarrow\psi)=1\wedge\frac{\pi(\psi)}{\pi(\theta)}\frac{f(x|\theta)\indic_{B(\psi,\Delta/2)}(\theta)}{f(x|\psi)\indic_{B(\theta,\Delta/2)}(\psi)}$.
    \end{itemize}
  \end{block}
  On note $P_{i,\Delta}$ et $P_{s,\Delta}$ les noyaux respectifs associés.
\end{frame}
\begin{frame}
  \frametitle{Algorithme ABC Shadow}
  On se donne $n,\Delta$ et un état initial $\theta_0$.
  \begin{block}{Procédure}
    \begin{enumerate}
\item Générer $x$ selon $p(\cdot|\theta_0)$
\item Faire $n$ itérations de la chaîne shadow, i.e. une réalisation de $P_{s,\Delta}^n(\theta_0,\cdot)$.
\item Retourner la $n$-ème itération notée $\theta_n$
  \item Retourner en 1. avec $\theta_0=\theta_n$
\end{enumerate}
  \end{block}
\end{frame}
\subsection{Résultats théoriques}
\begin{frame}
  \frametitle{Article original}
  \begin{block}{Contrôle de l'écart des noyaux}
      Soit $P_{i,\Delta}$ et $P_{s,\Delta}$ les noyaux des chaînes engendrées par le même couple $(x,\Delta)$. Alors il existe une constante $C_3>0$ qui dépend de $x$, $p$ et $\Theta$ telle que
  \begin{equation*}
    \forall n\in\N, \sup_{\theta\in\Theta}\left\|P_i^n(\theta,\cdot)-P_s^n(\theta,\cdot)\right\|_{TV}<C_3(x,p,\Theta)n\Delta.
  \end{equation*}
  \end{block}
\end{frame}
\begin{frame}
  \frametitle{Vers la convergence}
  \begin{block}{Contrôle uniforme de l'écart des noyaux}
    Faisons les hypothèses suivantes
    \begin{enumerate}
    \item $\theta\mapsto \E_\theta\left\|t\right\|$ est définie et bornée sur $\Theta$,\label{esp_norme1}
    \item $\theta\mapsto \E_\theta t$ est définie et bornée sur $\Theta$.\label{esp_norme2}
    \end{enumerate}
    Alors pour tout $\varepsilon>0$, il existe $C>0$ tel que
    \begin{equation*}
      \forall \psi\in\Theta,\ x\sim f(\cdot|\psi)\Rightarrow \P(C_3(x)\leq C)\geq 1-\varepsilon.
    \end{equation*}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Vers la convergence}
  Alors sous les mêmes hypothèses, 
  \begin{block}{Résultat asymptotique}
    Soit $\varepsilon, \delta>0$. Il existe une suite $(n_k,\Delta_k)_k$ telle que
    \begin{equation*}
      \P\left(\sup_{\theta\in\Theta}\left\|P_{s,\Delta_1}^{n_1}\cdots P_{s,\Delta_k}^{n_k}(\theta,\cdot)-\pi\right\|\leq\varepsilon\right)\geq 1-\delta
    \end{equation*}
    pour $k$ assez grand.
  \end{block}
  Résultat partiel.
\end{frame}
\begin{frame}
  \frametitle{Obstacles}
  Ergodicité uniforme de la chaîne idéale :
  \begin{equation*}
    \sup_{\theta}\left\|P_{i,\Delta}^n(\theta,\cdot)-\pi\right\|\leq 2\rho_{\Delta}^n
  \end{equation*}
  Convergence de la chaîne shadow :
  \begin{equation*}
    \sup_{\theta}\left\|P_{s,\Delta}^n(\theta,\cdot)-\pi\right\|\leq 2\rho_{\Delta}^n+Cn\Delta
  \end{equation*}
  Question : comment est-ce que
  \begin{equation*}
    \frac{\rho_\Delta}{\Delta}
  \end{equation*}
  se comporte ?
  \begin{alertblock}{Attention}
    Dépendance en la variable auxiliare.
  \end{alertblock}
\end{frame}
\section{Conclusion}
\begin{frame}
  Perspectives
  \begin{itemize}
  \item Lien ABC Shadow $\leftrightarrow$ gradient stochastique à discuter
    \item Progrès possibles vers un résultat de convergence en loi
  \end{itemize}
\end{frame}
\end{document}
